//
//  MaxScanViewController.m
//  2DCode
//
//  Created by max on 16/7/19.
//  Copyright © 2016年 Max Mak. All rights reserved.
//

#import "MaxScanViewController.h"
#define kViewbackColor [[UIColor blackColor]colorWithAlphaComponent:0.5]
#define kBtnDistance 100
#define kCGRectMake CGRectMake(0, 0, 60, 80)
#define kWindowSize [UIScreen mainScreen].bounds.size
#define kscanSize   CGSizeMake(kWindowSize.width*3/4, kWindowSize.width*3/4)
#define kCenterRect CGRectMake(0, 0, kscanSize.width, kscanSize.height)
#define kCenterPoint CGPointMake(CGRectGetMidX([UIScreen mainScreen].bounds), CGRectGetMidY([UIScreen mainScreen].bounds))

#ifdef DEBUG
#   define NSLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#   define NSLog(...)
#endif

@interface MaxScanViewController () <
AVCaptureMetadataOutputObjectsDelegate,
UIImagePickerControllerDelegate,
UINavigationControllerDelegate
>

@property (nonatomic, strong) MaxMainView *scanRectView;
@property (nonatomic,strong)UIView *naviView;  //导航栏
@property (nonatomic,strong)UIView *tabbarView;//TabbarView

@property (nonatomic,strong) AVCaptureDevice *captureDevice;   //获取设备
@property (nonatomic,strong) AVCaptureSession * captureSession; //输入
@property (nonatomic,strong) AVCaptureDeviceInput * deviceInput;// 输出
@property (nonatomic,strong) AVCaptureMetadataOutput *captureOutput;//链接输入和输出的桥梁
@property (strong, nonatomic) AVCaptureVideoPreviewLayer *preview;// 扫码的界面的layer



@property (nonatomic,copy) void(^blockVC)(UIViewController *vc);
@property (nonatomic,copy) void(^blockString)(NSString *string);


@end

@implementation MaxScanViewController




#pragma mark - Lazy Navi UIView
- (UIView *)naviView
{
    if (!_naviView) {
        
        _naviView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 64)];
        _naviView.backgroundColor = kViewbackColor;
        
        _naviView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        //标题
        UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(_naviView.frame.size.width/2-30, _naviView.frame.size.height/2-10, 60, 44)];
        title.text = @"扫一扫";
        title.textAlignment = NSTextAlignmentCenter;
        title.textColor = [UIColor whiteColor];
        title.font = [UIFont systemFontOfSize:19.f];
        [_naviView addSubview:title];
        title.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        
        //BackAction
        UIButton *backAction = [UIButton buttonWithType:UIButtonTypeCustom];
        [backAction setImage:[MaxScanViewController ReadImagePath:@"qrcode_scan_titlebar_back_nor@2x"] forState:UIControlStateNormal];
        [backAction setImage:[MaxScanViewController ReadImagePath:@"qrcode_scan_titlebar_back_pressed@2x"] forState:UIControlStateHighlighted];
        backAction.frame = CGRectMake(10, 20, 30, 40);
        [_naviView addSubview:backAction];
        
        [backAction addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
        
    }


    return _naviView;

}



#pragma mark - Lazy TabbarView
- (UIView *)tabbarView
{

    if (!_tabbarView) {
        
        _tabbarView = [[UIView alloc]initWithFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height - 80, self.naviView.bounds.size.width, 80)];
        _tabbarView.backgroundColor = kViewbackColor;
        _tabbarView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        
        //闪光灯
        UIButton *lampBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        lampBtn.frame = kCGRectMake;
        [lampBtn setImage:[MaxScanViewController ReadImagePath:@"qrcode_scan_btn_flash_nor@2x"] forState:UIControlStateNormal];
        [lampBtn setImage:[MaxScanViewController ReadImagePath:@"qrcode_scan_btn_scan_off@2x"] forState:UIControlStateHighlighted];
        [lampBtn setImage:[MaxScanViewController ReadImagePath:@"qrcode_scan_btn_scan_off@2x"] forState:UIControlStateSelected];
//        lampBtn.center = CGPointMake(_tabbarView.frame.size.width/2, _tabbarView.frame.size.height/2);
        lampBtn.center = CGPointMake(_tabbarView.frame.size.width/2+50, _tabbarView.frame.size.height/2);
        [lampBtn addTarget:self action:@selector(lampAction:) forControlEvents:UIControlEventTouchUpInside];
        [_tabbarView addSubview:lampBtn];

        
        //相册
        UIButton *ImageBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        ImageBtn.frame = kCGRectMake;
        [ImageBtn setImage:[MaxScanViewController ReadImagePath:@"qrcode_scan_btn_photo_nor@2x"] forState:UIControlStateNormal];
        [ImageBtn setImage:[MaxScanViewController ReadImagePath:@"qrcode_scan_btn_photo_down@2x"] forState:UIControlStateHighlighted];
        [ImageBtn addTarget:self action:@selector(imageAction:) forControlEvents:UIControlEventTouchUpInside];
        [_tabbarView addSubview:ImageBtn];
//        ImageBtn.center = CGPointMake(lampBtn.center.x-kBtnDistance, lampBtn.center.y);
        ImageBtn.center = CGPointMake(_tabbarView.frame.size.width/2-50, _tabbarView.frame.size.height/2);
#if 0
        //my code
        UIButton *myBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        myBtn.frame = kCGRectMake;
        [myBtn setImage:[MaxScanViewController ReadImagePath:@"qrcode_scan_btn_myqrcode_nor@2x"] forState:UIControlStateNormal];
        [myBtn setImage:[MaxScanViewController ReadImagePath:@"qrcode_scan_btn_myqrcode_down@2x"] forState:UIControlStateHighlighted];
        [myBtn addTarget:self action:@selector(myAction:) forControlEvents:UIControlEventTouchUpInside];
        [_tabbarView addSubview:myBtn];
        myBtn.center = CGPointMake(lampBtn.center.x+kBtnDistance, lampBtn.center.y);
        
#endif
        
        
    }
    return _tabbarView;

}

#pragma mark - SacnRectView
- (MaxMainView *)scanRectView
{
    if (!_scanRectView) {
        
        _scanRectView = [[MaxMainView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
//        _scanRectView.frame = CGRectMake(0, 0, kscanSize.width, kscanSize.height);
//        _scanRectView.center = CGPointMake(CGRectGetMidX([UIScreen mainScreen].bounds), CGRectGetMidY([UIScreen mainScreen].bounds));
    }


    return _scanRectView;
}


#pragma mark - captureDevice
-(AVCaptureDevice *)captureDevice
{
    //获取摄像设备
    if(_captureDevice == nil)
    {
        _captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    }
    return _captureDevice;
}

#pragma mark - addView Method
- (void)addView{
     [self.view addSubview:self.scanRectView];
    [self.view addSubview:self.naviView];
    [self.view addSubview:self.tabbarView];
   
    
}

- (void)setUp{

    CGRect scanRect = CGRectMake((kWindowSize.width-kscanSize.width)/2, (kWindowSize.height-kscanSize.height)/2, kscanSize.width, kscanSize.height);
    
    //创建输入流
    self.deviceInput = [AVCaptureDeviceInput deviceInputWithDevice:self.captureDevice error:nil];
    //创建输出流
    self.captureOutput = [[AVCaptureMetadataOutput alloc]init];
     //设置代理 在主线程里刷新
    [self.captureOutput setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
     //初始化链接对象
    self.captureSession = [[AVCaptureSession alloc]init];
    //高质量采集率
    [self.captureSession setSessionPreset:([UIScreen mainScreen].bounds.size.height<500)?AVCaptureSessionPreset640x480:AVCaptureSessionPresetHigh];
    [self.captureSession addInput:self.deviceInput];
    [self.captureSession addOutput:self.captureOutput];
    //设置扫码支持的编码格式(如下设置条形码和二维码兼容)
    self.captureOutput.metadataObjectTypes=@[AVMetadataObjectTypeQRCode,AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeEAN8Code, AVMetadataObjectTypeCode128Code];
    self.captureOutput.rectOfInterest = scanRect;
    
    self.preview = [AVCaptureVideoPreviewLayer layerWithSession:self.captureSession];
    self.preview.videoGravity = AVLayerVideoGravityResizeAspectFill;
    self.preview.frame = [UIScreen mainScreen].bounds;
    [self.view.layer insertSublayer:self.preview atIndex:0];
    
      //开始捕获
    [self.captureSession startRunning];

    
    //rectOfInterest的值的范围都是0-1 是按比例取值而不是实际尺寸 不过其实也很简单 只要换算一下就好了 接下来我们添加取景框
    
    //计算rectOfInterest 注意x,y交换位置
    scanRect = CGRectMake(scanRect.origin.y/kWindowSize.height, scanRect.origin.x/kWindowSize.width, scanRect.size.height/kWindowSize.height,scanRect.size.width/kWindowSize.width);
    self.captureOutput.rectOfInterest = scanRect;
    
    


}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addView];
    [self setUp];
    [self.scanRectView startAnimating];
    

}
- (UIStatusBarStyle)preferredStatusBarStyle{

    return UIStatusBarStyleLightContent;

}


- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection{

    if ( (metadataObjects.count==0) )
    {
        return;
    }
    if (metadataObjects.count >0) {
        
        
        
        AVMetadataMachineReadableCodeObject *metadataObject = metadataObjects.firstObject;
        //输出扫描字符串
        NSLog(@"%@",metadataObject.stringValue);
        
        if (self.blockString) {
            self.blockString(metadataObject.stringValue);
        }
        [self.scanRectView stopAnimating];
        [self.captureSession stopRunning];
    }



}

#pragma mark - Read UIIamge
+ (UIImage *)ReadImagePath:(NSString *)name{

    
    NSString *imagePath = [[NSBundle mainBundle]pathForResource:@"CodeScan" ofType:@"bundle"];
    if ([imagePath length] > 0) {
        
        NSBundle *imageBundle = [NSBundle bundleWithPath:imagePath];
        UIImage *image = [UIImage imageWithContentsOfFile:[imageBundle pathForResource:name ofType:@"png"]];
        
        return image;
        
    }

    return nil;
}

#pragma mark - BackButton Method
- (void)backAction:(id)sender{


    NSLog(@"%s",__FUNCTION__);
    if (self.blockVC) {
        self.blockVC(self);
    }

    


}

- (void)sendQRCodeString:(void (^)(NSString *))block
{
    
    if (self.blockString) {
        self.blockString = [block copy];
    }



}

#pragma mark - Tabbar Button Method
- (void)lampAction:(UIButton *)sender{
    
    NSLog(@"%s",__FUNCTION__);
    //是否发开闪光灯
    [self.captureDevice lockForConfiguration:nil];
    [self.captureDevice setTorchMode:!sender.selected?AVCaptureTorchModeOn:AVCaptureTorchModeOff];
    [self.captureDevice setFlashMode:!sender.selected?AVCaptureFlashModeOn:AVCaptureFlashModeOff];
    sender.selected = !sender.selected;

}

- (void)imageAction:(id)sender{

    NSLog(@"%s",__FUNCTION__);
    
    UIImagePickerController *picker = [[UIImagePickerController alloc]init];
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    picker.allowsEditing = YES;
    
    picker.delegate = self;
    
    [self presentViewController:picker animated:YES completion:^{
        
    }];
    


}


- (void)myAction:(id)sender{


    NSLog(@"%s",__FUNCTION__);
    
    
    
    

}


#pragma mark - UIImagePickerControllerDelegate

-(void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    
    [self ScaningLactionImage:image];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        
    }];



}

#pragma mark - BackActionsBlock

- (void)backButtonMethod:(void (^)(UIViewController *))block
{
        if (block) {
        self.blockVC = [block copy];
    }

}

- (void)ScaningLactionImage:(UIImage *)image {

    //初始化检测器
    CIDetector *detector=[CIDetector detectorOfType:CIDetectorTypeQRCode context:nil options:@{CIDetectorAccuracy:CIDetectorAccuracyHigh}];
    
    //转成CIImage
    CIImage *ciimage=[CIImage imageWithCGImage:image.CGImage];
    
    //检测结果
    NSArray *features =[detector featuresInImage:ciimage];
    CIQRCodeFeature *feature=[features firstObject];
    
    //打印
    NSString *string=[feature messageString];
    
    NSLog(@"%@",string);
    
    if (string) {
    
        if (self.blockString) {
            self.blockString(string);
        }
        
        
    }else{
#pragma clang diagnostic push
        
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        //提示
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"error" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
#pragma clang diagnostic pop
        
        [alert show];
        
    }
    
}


#pragma mark - 生成二维码

/**
 *  生成二维码
 */
+ (UIImage *)creatCIQRCodeImage:(NSString *)creatString withSize:(CGFloat)size
{
    
    CIFilter *filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    
    
    [filter setDefaults];
    
    NSString *dataString = creatString;
    NSData *data = [dataString dataUsingEncoding:NSUTF8StringEncoding];
    
    [filter setValue:data forKeyPath:@"inputMessage"];
    
    CIImage *outputImage = [filter outputImage];
    
    UIImage *image = [MaxScanViewController creatNonInterpolatedUIImageFormCIImage:outputImage withSize:size];
    
    [MaxScanViewController saveImage:image WithName:@"qr.png"];
    return image;
}


/**
 *  根据CIImage生成指定大小的UIImage
 *
 *  @param image CIImage
 *  @param size  图片宽度
 *
 *  @return 生成高清的UIImage
 */
+ (UIImage *)creatNonInterpolatedUIImageFormCIImage:(CIImage *)image withSize:(CGFloat)size
{
    CGRect extent = CGRectIntegral(image.extent);
    CGFloat scale = MIN(size/CGRectGetWidth(extent), size/CGRectGetHeight(extent));
    
    size_t width = CGRectGetWidth(extent) * scale;
    size_t height = CGRectGetHeight(extent) * scale;
    CGColorSpaceRef cs = CGColorSpaceCreateDeviceGray();
    CGContextRef bitmapRef = CGBitmapContextCreate(nil, width, height, 8, 0, cs, (CGBitmapInfo)kCGImageAlphaNone);
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef bitmapImage = [context createCGImage:image fromRect:extent];
    CGContextSetInterpolationQuality(bitmapRef, kCGInterpolationNone);
    CGContextScaleCTM(bitmapRef, scale, scale);
    CGContextDrawImage(bitmapRef, extent, bitmapImage);
    
    
    CGImageRef scaledImage = CGBitmapContextCreateImage(bitmapRef);
    CGContextRelease(bitmapRef);
    CGImageRelease(bitmapImage);
    return [UIImage imageWithCGImage:scaledImage];
}


/**
 异步保存图片

 @param tempImage 图片
 @param imageName 名字
 */
+ (void)saveImage:(UIImage *)tempImage WithName:(NSString *)imageName
{
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        NSData *imageData = UIImagePNGRepresentation(tempImage);
        
        NSString *docmentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
        
        NSString *imagePath = [docmentsPath stringByAppendingPathComponent:imageName];
        
        [imageData writeToFile:imagePath atomically:NO];
        
        [[NSUserDefaults standardUserDefaults]setObject:imageName forKey:kQRImagePath];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
    });
}


/**
 路径

 @return 二维码路径
 */
+ (NSString *)documentQRImagePath{
    NSString *imageName = [[NSUserDefaults standardUserDefaults]stringForKey:kQRImagePath];
    return [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:imageName];
}

#pragma mark - System Method

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}

- (void)viewDidDisappear:(BOOL)animated
{

    [super viewDidDisappear:animated];
    
}

- (void)dealloc
{
    NSLog(@"%s",__FUNCTION__);

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end


#pragma mark - MainView
@interface MaxMainView()
@property (nonatomic,strong)UIImageView *LineImage;
@property (nonatomic,strong)MaxScanView *backView;

@end
@implementation MaxMainView


- (UIImageView *)LineImage
{

    if (!_LineImage) {
        _LineImage = [[UIImageView alloc]init];
        _LineImage.image = [MaxScanViewController ReadImagePath:@"qrcode_scan_light_green@2x"];
        _LineImage.hidden = YES;
        _LineImage.frame = CGRectMake(0, 0, kCenterRect.size.width-20, 20);
        [self addSubview:_LineImage];
        
    }
    return _LineImage;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
        self.backView = [[MaxScanView alloc]initWithFrame:frame];
        [self addSubview:self.backView];
        
    }


    return self;
}

#pragma mark - Animating
- (void)startAnimating{
    
   __block CGPoint point = self.center;
    point.y = point.y - kCenterRect.size.height/2+10;
    
    self.LineImage.center = point;

    self.LineImage.alpha = 0.0;
    self.LineImage.hidden = NO;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.LineImage.alpha = 1.0;
    }];
    
    [UIView animateWithDuration:2.0 animations:^{
        
        point.y = point.y + kCenterRect.size.height-20;
        
        
        self.LineImage.center = point;
        
        
    } completion:^(BOOL finished) {
        
        self.LineImage.hidden = YES;
        [self performSelector:@selector(startAnimating) withObject:nil afterDelay:0.3];
        
        
    }];
    
}


- (void)stopAnimating{
    
    [self.LineImage removeFromSuperview];
    
}

@end


#pragma mark - MaxScanView Class

@interface MaxScanView()
@property (nonatomic,strong)UILabel *title;

@end

@implementation MaxScanView

- (UILabel *)title
{

    if (!_title) {
        _title = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, 50)];
        _title.text = @"将取景框对准二维码\n即可自动扫描";
        _title.textColor = [UIColor whiteColor];
        _title.textAlignment = NSTextAlignmentCenter;
        
        CGPoint point = self.center;
        point.y = kCenterPoint.y-kCenterRect.size.height/2-30;
        _title.center = point;
        _title.numberOfLines = 0;
    }

    return _title;

}


- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
        self.backgroundColor = kViewbackColor;
        [self addSubview:self.title];
        
        
    }

    return self;
}

- (void)drawRect:(CGRect)rect
{

    [self drawScanRect];

}

//setting Center View
- (void)drawScanRect{
    
    
    CAShapeLayer *shapeLayer = [[CAShapeLayer alloc]init];
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddRect(path, nil, CGRectMake(0, 0, self.frame.size.width, kCenterPoint.y-kCenterRect.size.height/2));
    CGPathAddRect(path, nil, CGRectMake(0, kCenterPoint.y-kCenterRect.size.height/2, (self.frame.size.width-kCenterRect.size.width)/2, kCenterRect.size.height));
    CGPathAddRect(path, nil, CGRectMake((self.frame.size.width-kCenterRect.size.width)/2+kCenterRect.size.width, kCenterPoint.y-kCenterRect.size.height/2,  (self.frame.size.width-kCenterRect.size.width)/2, kCenterRect.size.height));
    
    CGRect rect = CGRectMake((self.frame.size.width-kCenterRect.size.width)/2+kCenterRect.size.width, kCenterPoint.y-kCenterRect.size.height/2,  (self.frame.size.width-kCenterRect.size.width)/2, kCenterRect.size.height);
    
    CGPathAddRect(path, nil, CGRectMake(0, CGRectGetMaxY(rect), self.frame.size.width, self.frame.size.height -CGRectGetMaxY(rect)));
                  
    shapeLayer.path = path;
    self.layer.mask = shapeLayer;
    CGPathRelease(path);
    
    
    
    //center View
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    {
    
        CGContextSetStrokeColorWithColor(context, [UIColor whiteColor].CGColor);
        CGContextSetLineWidth(context, 1);
        CGContextAddRect(context, CGRectMake((self.frame.size.width-kCenterRect.size.width)/2, kCenterPoint.y-kCenterRect.size.height/2, kCenterRect.size.width, kCenterRect.size.height));
        CGContextStrokePath(context);
    
    }
    {
    
        
        CGRect rect1 = CGRectMake((self.frame.size.width-kCenterRect.size.width)/2, kCenterPoint.y-kCenterRect.size.height/2-0.08, kCenterRect.size.width, kCenterRect.size.height);
        
        CGContextSetStrokeColorWithColor(context, [UIColor colorWithRed:0. green:167./255. blue:231./255. alpha:1.0].CGColor);
        CGContextSetLineWidth(context, 4);
        //left top top
        CGContextMoveToPoint(context, (self.frame.size.width-kCenterRect.size.width)/2-4, kCenterPoint.y-kCenterRect.size.height/2-2);
        CGContextAddLineToPoint(context, (self.frame.size.width-kCenterRect.size.width)/2+24, kCenterPoint.y-kCenterRect.size.height/2-2);
        
        //left top bottom
        CGContextMoveToPoint(context, (self.frame.size.width-kCenterRect.size.width)/2-2, kCenterPoint.y-kCenterRect.size.height/2-4);
        CGContextAddLineToPoint(context, (self.frame.size.width-kCenterRect.size.width)/2-2, kCenterPoint.y-kCenterRect.size.height/2+24);
        
        //left bottom top
        
        CGContextMoveToPoint(context, (self.frame.size.width-kCenterRect.size.width)/2-2, CGRectGetMaxY(rect1)-20);
        CGContextAddLineToPoint(context, (self.frame.size.width-kCenterRect.size.width)/2-2, CGRectGetMaxY(rect1)+4);
        
        //left bottom bottom
        
        CGContextMoveToPoint(context, rect1.origin.x-4, CGRectGetMaxY(rect1)+2);
        CGContextAddLineToPoint(context, rect1.origin.x+24, CGRectGetMaxY(rect1)+2);
        
        //right top top
        
        CGContextMoveToPoint(context, CGRectGetMaxX(rect1)-20, rect1.origin.y-2);
        CGContextAddLineToPoint(context, CGRectGetMaxX(rect1)+4, rect1.origin.y-2);
        
        //right top bottom
        
        CGContextMoveToPoint(context, CGRectGetMaxX(rect1)+2, rect1.origin.y-4);
        CGContextAddLineToPoint(context, CGRectGetMaxX(rect1)+2, rect1.origin.y+24);
        
        //right bottom top
        
        CGContextMoveToPoint(context, CGRectGetMaxX(rect1)+2, CGRectGetMaxY(rect1)-20);
        CGContextAddLineToPoint(context, CGRectGetMaxX(rect1)+2, CGRectGetMaxY(rect1)+4);
        
        //right bottom bottom
        
        CGContextMoveToPoint(context, CGRectGetMaxX(rect1)-20, CGRectGetMaxY(rect1)+2);
        CGContextAddLineToPoint(context, CGRectGetMaxX(rect1)+4, CGRectGetMaxY(rect1)+2);
        
        
        CGContextStrokePath(context);
    
    
    }
    


}




@end
#pragma mark - MyQRCodeViewController Class
@implementation MyQRCodeViewController





@end

