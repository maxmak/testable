//
//  MaxScanViewController.h
//  2DCode
//
//  Created by max on 16/7/19.
//  Copyright © 2016年 Max Mak. All rights reserved.
//
/**
 *  支持iOS8以上
 *  作者：Max
 *  扫描二维码
 *  presentViewController MaxScanViewController
 */
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

#define kQRImagePath @"QRImagePath" //二维码路径
@interface MaxScanViewController : UIViewController


/**
 *  返回按钮
 *
 *  @param block 回调Self
 */
- (void)backButtonMethod:(void(^)(UIViewController *))block;
/**
 *  扫描成功回调
 *
 *  @param block
 */
- (void)sendQRCodeString:(void(^)(NSString *))block;



/**
 生成二维码

 @param creatString 字符串
 @param size        图片大小

 @return 生成后二维码
 */
+ (UIImage *)creatCIQRCodeImage:(NSString *)creatString withSize:(CGFloat)size;


/**
 保存二维码

 @param tempImage 二维码
 @param imageName 名字
 */
+ (void)saveImage:(UIImage *)tempImage WithName:(NSString *)imageName;


/**
 二维码在沙盒路径

 @return 路径
 */
+ (NSString *)documentQRImagePath;

@end



@interface MyQRCodeViewController : UIViewController



@end





@interface MaxMainView : UIView

- (void)startAnimating;
- (void)stopAnimating;

@end
//Background
@interface MaxScanView : UIView




@end




