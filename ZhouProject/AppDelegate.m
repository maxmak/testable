//
//  AppDelegate.m
//  ZhouProject
//
//  Created by MaxMak on 2017/8/24.
//  Copyright © 2017年 MaxMak. All rights reserved.
//

#import "AppDelegate.h"
#import "WXApi.h"
#import "NSObject+Extension.h"
#import "WXApiManager.h"
#import "ViewController.h"
#import "MaxAlertViewManager.h"
@interface AppDelegate ()

@end

@implementation AppDelegate


- (void)setWx{
    
    [WXApi registerApp:[AppDelegate getWXSchemes]];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
//    [self setWx];
    return YES;
}


- (void)applicationWillEnterForeground:(UIApplication *)application{

    
    ViewController *vc = (ViewController *)self.window.rootViewController;

    NSLog(@"123");
    NSLog(@"%@",vc.webView.URL.absoluteString);
    
//    if ([vc.webView.URL.absoluteString containsString:@"payment"]) {
//        dispatch_async(dispatch_get_main_queue(), ^{
           [vc.webView alipayResults];
//        });
//    }
    


    
}





- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url{

    return [WXApi handleOpenURL:url delegate:[WXApiManager sharedManager]];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    
    return [WXApi handleOpenURL:url delegate:[WXApiManager sharedManager]];

}




@end
