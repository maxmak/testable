//
//  OnlinePayManager.m
//  ZhouProject
//
//  Created by MaxMak on 2017/8/24.
//  Copyright © 2017年 MaxMak. All rights reserved.
//

#import "OnlinePayManager.h"
#import "HttpRequest.h"

#import "NSObject+Extension.h"
#import <objc/message.h>
static NSString *TONGLIANURL = @"http://113.108.182.3:10080/apiweb/weixin/";
// http://113.108.182.3:10080/apiweb/weixin





@implementation OnlinePayManager


+ (NSString *)getTonglianUrlStyle:(TongLianStyle)style{
    
    NSString *inface;
    
    switch (style) {
        case pay:
            inface = @"pay";
            break;
        case cancel:
            inface = @"cancel";
            break;
        case refund:
            inface = @"refund";
        case query:
            inface = @"query";
            break;
        case closes:
            inface = @"closes";
            break;
    }
    
    
    return [NSString stringWithFormat:@"%@%@",TONGLIANURL,inface];
}

+ (OnlinePayManager *)manager{
    static OnlinePayManager * manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[OnlinePayManager alloc] init];
    });
    
    return manager;
}


- (void)getTonglianPay:(TonglianPayModel  * _Nonnull  )model
               success:(void(^)(id responseObject))success
               failure:(void(^)(NSError *error))failure{
    
//    NSDictionary *dict = [model dictionaryRepresentation:nil];
//    
//    [HttpRequest POSTwithURL:[OnlinePayManager getTonglianUrlStyle:pay] params:dict success:success failure:failure];
}


@end
