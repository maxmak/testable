#import "MainWebView.h"
#import <JavaScriptCore/JavaScriptCore.h>
#import "MaxAlertViewManager.h"
#import <WebViewJavascriptBridge/WKWebViewJavascriptBridge.h>
#import "HttpRequest.h"
#import <AFNetworking/AFNetworking.h>
#import <AlipaySDK/AlipaySDK.h>
#import "MaxDefineHeader.h"

static NSString * noSession = @"-1";
static NSString * sessionKey = @"SESSION";

static NSString * account_key = @"accountkey";
static NSString * password_key = @"password_key";
@interface MainWebViewDelegate : NSObject<WKNavigationDelegate,WKUIDelegate>

@end


@implementation MainWebViewDelegate

@end


@interface MainWebView()
<WKNavigationDelegate,
WKUIDelegate,
WKScriptMessageHandler>
@property (strong,nonatomic) MainWebViewDelegate *dele;
@property (assign,nonatomic) BOOL *isFirstLoaded;
@property (strong,nonatomic) NSString *session;
@property (assign,nonatomic) BOOL loadState;
@property (strong,nonatomic) WKWebViewJavascriptBridge *bridge;
@property (strong,nonatomic) NSString *memberSessionSd;
@property (strong,nonatomic) NSString *historyUrl;
@end

@implementation MainWebView
{
    NSString *_memberSessionSd;
}


- (void)setMemberSessionSd:(NSString *)memberSessionSd{

    [[NSUserDefaults standardUserDefaults] setObject:memberSessionSd forKey:@"userId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    _memberSessionSd = memberSessionSd;
}

- (NSString *)memberSessionSd{
    
    if (!_memberSessionSd) {
        _memberSessionSd = [[NSUserDefaults standardUserDefaults]stringForKey:@"userId"];
    }
    
    return (_memberSessionSd != nil && ![_memberSessionSd isEqualToString:@""]) ? _memberSessionSd:nil;
}

- (MainWebViewDelegate *)dele{
    return _dele ? _dele : [[MainWebViewDelegate alloc] init];
}


- (void)setSession:(NSString *)session{

    
    void(^block)() = ^{
        [[NSUserDefaults standardUserDefaults]setValue:session forKey:sessionKey];
//        [[NSUserDefaults standardUserDefaults]setObject:session forKey:sessionKey];
        [[NSUserDefaults standardUserDefaults]synchronize];
    };
    
    if (!_session) {
        NSString * sess = [[NSUserDefaults standardUserDefaults]stringForKey:sessionKey];
        if (sess) {
            if (![sess isEqualToString:session]) {
                block();
            }
        }else{
            block();
        }
    }
    
    if (![_session isEqualToString:session]) {
        block();
    }
    
    _session = session;
}

- (instancetype)initWithFrame:(CGRect)frame{

    if (self = [super initWithFrame:frame]) {
        self.allowsBackForwardNavigationGestures = YES;
        self.navigationDelegate = self;
        self.UIDelegate = self;
        self.bridge = [WKWebViewJavascriptBridge bridgeForWebView:self];
        [self.bridge setWebViewDelegate:self];
        [WKWebViewJavascriptBridge enableLogging];
        
    }
    return self;
}


- (instancetype)initWithFrame:(CGRect)frame configuration:(WKWebViewConfiguration *)configuration{

    if (self = [super initWithFrame:frame configuration:configuration]) {
        self.allowsBackForwardNavigationGestures = YES;
        self.navigationDelegate = self;
        self.UIDelegate = self.dele;
        self.bridge = [WKWebViewJavascriptBridge bridgeForWebView:self];
        [self.bridge setWebViewDelegate:self];
        [WKWebViewJavascriptBridge enableLogging];
        
        WKUserContentController *userCC = configuration.userContentController;
        
        [userCC addScriptMessageHandler:self name:@"pay"];
        
        [self addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:nil];
    

     
    }
    return self;
}

+ (BOOL)isUrl:(NSString *)url{
    
    NSString *regex = @"[a-zA-z]+://[^\\s]*";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [pred evaluateWithObject: url];
}


+(NSString *)getJSESSIONID{
    
    NSString * session = [[NSUserDefaults standardUserDefaults]stringForKey:sessionKey];
    if (!session) {
        return noSession;
    }
    return [NSString stringWithFormat:@"%@=%@",sessionKey,session];
}

+ (WKWebViewConfiguration *)getWKWebViewConfiguration{
    
    WKUserContentController* userContentController = WKUserContentController.new;
    
    WKWebViewConfiguration* webViewConfig = WKWebViewConfiguration.new;
    
    webViewConfig.userContentController = userContentController;
    
    WKPreferences *preferences = [[WKPreferences alloc] init];
    
    preferences.javaScriptCanOpenWindowsAutomatically = YES;
    
//    preferences.minimumFontSize = 30.0;
    
    webViewConfig.preferences = preferences;
    
    return  webViewConfig;
}

- (BOOL)max_loadRequest:(NSString *)url{
    
    if (![MainWebView isUrl:url]) {return NO;}

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];

    [self loadRequest:request];
    
    return YES;
}

-(void)max_goBack:(NSURL *)url execution:(void(^)())block{

    if ([url.absoluteString hasSuffix:@"javascript%20:;"]) {
        [self goBack];
        if (block) {block();}
    }
    
}

- (NSString *)getOrderId:(NSString *)url{
    
    NSError *error;
    // 创建NSRegularExpression对象并指定正则表达式
    NSString * res;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"id=(.+?)&" options:0 error:&error];
    if (!error) { // 如果没有错误
        // 获取特特定字符串的范围
        
        NSTextCheckingResult *match = [regex firstMatchInString:url
                                                        options:0
                                                          range:NSMakeRange(0, [url length])];
        if (match) {
            // 截获特定的字符串
            NSString *result = [url substringWithRange:match.range];
            NSLog(@"%@",result);
            NSLog(@"%@",[result substringWithRange:NSMakeRange(3, result.length - 4)]);
            res = [result substringWithRange:NSMakeRange(3, result.length - 4)];
        }
    } else { // 如果有错误，则把错误打印出来
        NSLog(@"error - %@", error);
    }
    
    return res;
}

- (void)orderId:(NSString *)order complie:(void(^)(NSString *res))com{

    NSURLSession *session=[NSURLSession sharedSession];
    
    NSString *urlString=@"http://wx.qcrj.shop/web/shop/order/queryOrderStatus.htm";
    
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    request.HTTPMethod=@"POST";
    
    request.HTTPBody=[[NSString stringWithFormat:@"id=%@",order] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionDataTask *task=[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        NSString * str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        if ([@"1" isEqualToString:str]) {
            com(@"http://wx.qcrj.shop/web/shop/order/orderSuc.htm?client_device=ios");
        }else{
            com(@"http://wx.qcrj.shop/action/web/shop/order/order_payment?client_device=ios");
        }
        
    }];
    
    [task resume];
    

}

//MARK: 支付宝 回调

- (void)alipayResults{
    
    [self.bridge callHandler:@"alipayResults"];
    __weak typeof(self)weakSlef = self;
    
//    [self.bridge callHandler:@"alipayResults" data:nil responseCallback:^(id responseData) {
//       
//        NSLog(@"%@",responseData);
//        NSLog(@"asd");
//        
//        
//    }];
    
    if ([self.URL.absoluteString containsString:@"https://ds.alipay.com/"] && self.historyUrl) {
        [self orderId:self.historyUrl complie:^(NSString *res) {
            dispatch_async(dispatch_get_main_queue(), ^{
                    [weakSlef loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:res]]];
            });
            
        }];
        
    }


}

#pragma mark - WKNavigationDelegate

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
//    self.loadState = YES;
    NSLog(@"didFinishNavigation---%@",webView.URL.absoluteString);
    
    
    
//    [self autoLogin:webView.URL.absoluteString];
    
    
//    if ([webView.URL.absoluteString containsString:@"toLangZiCode"]) {
//
//    }
//    __weak typeof(self)weakSelf = self;
//    [self.bridge registerHandler:@"wechatShareToCircleFromJs" handler:^(id data, WVJBResponseCallback responseCallback) {
//        if (weakSelf.showDelegate && [weakSelf.showDelegate respondsToSelector:@selector(sharedARCode:dict:)]) {
//            [weakSelf.showDelegate sharedARCode:YES dict:data];
//        }
//    }];
//    [self.bridge registerHandler:@"wechatShareToFriendFromJs" handler:^(id data, WVJBResponseCallback responseCallback) {
//
//        NSLog(@"%@",data);
//        if (weakSelf.showDelegate && [weakSelf.showDelegate respondsToSelector:@selector(sharedARCode:dict:)]) {
//            [weakSelf.showDelegate sharedARCode:NO dict:data];
//        }
//
//    }];
//
//    [self.bridge registerHandler:@"scanQRCodefunc" handler:^(id data, WVJBResponseCallback responseCallback) {
//        if (weakSelf.showDelegate && [weakSelf.showDelegate respondsToSelector:@selector(getScanVC)]) {
//            [weakSelf.showDelegate getScanVC];
//        }
//
//    }];
//
//    if ([webView.URL.absoluteString containsString:@"toLangZiCode"]) {
//        [self evaluateJavaScript:@"document.getElementById('sao').visibility == 'visible'" completionHandler:^(id _Nullable data, NSError * _Nullable error) {
//
//        }];
//    }
    
    [self.bridge registerHandler:@"saveMemberInfoFromJs" handler:^(id data, WVJBResponseCallback responseCallback) {
       
        NSLog(@"saveMemberInfoFromJs %@",data);
        self.memberSessionSd = data[@"sessionId"];
    }];
    
    
    
}





- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation{
}

- (void)webView:(WKWebView *)webView didFailNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error{
    
        self.loadState = false;
    NSLog(@"didFailNavigation");
}

- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error{
    
    NSLog(@"didFailProvisionalNavigation--%@",webView.URL.absoluteString);
    
    /**
     2017-09-23 23:30:48.645125+0800 ZhouProject[873:116752] -[MainWebView webView:didFailProvisionalNavigation:withError:] [Line 350] 加载错误 {
     NSErrorFailingURLKey = "http://wx.qcrj.shop/?client_device=ios&member_session_id=8a9f95b45e11ec8b015e1486f53e1a8a";
     NSErrorFailingURLStringKey = "http://wx.qcrj.shop/?client_device=ios&member_session_id=8a9f95b45e11ec8b015e1486f53e1a8a";
     NSLocalizedDescription = "The Internet connection appears to be offline.";
     NSUnderlyingError = "Error Domain=kCFErrorDomainCFNetwork Code=-1009 \"(null)\" UserInfo={_kCFStreamErrorCodeKey=50, _kCFStreamErrorDomainKey=1}";
     "_WKRecoveryAttempterErrorKey" = "<WKReloadFrameErrorRecoveryAttempter: 0x170223040>";
     "_kCFStreamErrorCodeKey" = 50;
     "_kCFStreamErrorDomainKey" = 1;
     }
     */
    
    if (error && error.userInfo) {
        
        NSString *UnderlyingError = error.userInfo[@"NSUnderlyingError"];
        
        if (UnderlyingError) {
            NSString *ErrorFailingURLStringKey = error.userInfo[@"NSErrorFailingURLStringKey"];
            if (!ErrorFailingURLStringKey) {
                return;
            }
            if (_showDelegate && [self.showDelegate respondsToSelector:@selector(goError:)]) {
                [self.showDelegate goError:ErrorFailingURLStringKey];
            }
            
        }
        
    }
        NSLog(@"加载错误 %@",error.userInfo);

    
    
    self.loadState = false;
    //MARK: 失败回调
  
}

- (void)webViewWebContentProcessDidTerminate:(WKWebView *)webView{

    //window.webkit.messageHandlers.pay.postMessage({orderId:'',price:''});
    
}



- (void)alipayClick:(NSURL *)url complie:(void(^)())com{
    //[[url absoluteString] hasPrefix:@"alipays://"]

        NSString *str = [url.absoluteString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    if ([str containsString:@"alipays://"]) {
        
        NSRange range = [str rangeOfString:@"alipays://"]; //截取的字符串起始位置
        NSString * resultStr = [str substringFromIndex:range.location]; //截取字符串
        
        NSURL *alipayURL = [NSURL URLWithString:resultStr];
        
//        if ([[UIApplication sharedApplication] canOpenURL:alipayURL]) {
            [[UIApplication sharedApplication]openURL:alipayURL];
//        }
        
        com();
        
    }
    if ([[url absoluteString] hasPrefix:@"itms-apps://"]) {
        [[UIApplication sharedApplication] openURL:url];
    }

}
#pragma mark - 退出登录
- (void)outLogin:(NSString *)url{
    
    if ([[url lowercaseString] containsString:@"exit"]) {
//        [self deleteAccount];
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"userId"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        _memberSessionSd = nil;
    }
    
}

- (void)alipaysClick:(NSURL *)url{

    if ([[url absoluteString] hasPrefix:@"alipays://"]) {
        //MARK: 支付宝
        [self alipayClick:url complie:^{}];
    }
    
    if ([[url absoluteString] hasPrefix:@"itms-apps://"]) {
        [[UIApplication sharedApplication] openURL:url];
    }
    
}
#pragma mark - 获取session_member_id
- (void)max_sessionMemberId:(NSURL *)url{

    if (self.memberSessionSd) {
        return;
    }
    
    if ([[url absoluteString] containsString:@"session_member_id"]) {
        NSRange range = [[url absoluteString] rangeOfString:@"session_member_id"];
        NSString * resultStr = [url.absoluteString substringFromIndex:range.location];
        NSLog(@"%@",resultStr);
        NSString * resultStr1 = [resultStr substringFromIndex:18];
        NSLog(@"%@",resultStr1);
        self.memberSessionSd = resultStr1;
        
    }
    
}

- (void)webView:(WKWebView *)webView didReceiveServerRedirectForProvisionalNavigation:(null_unspecified WKNavigation *)navigation{

}




- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler{
    
    
    
    decisionHandler(WKNavigationResponsePolicyAllow);
}


#pragma mark - 重点
- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler{
    
 
    
    NSLog(@"decisionHandler----%@",webView.URL.absoluteString);
    
    NSLog(@"as --%@",[navigationAction.request.URL absoluteString]);
    

    
//    NSString *absStr = [navigationAction.request.URL absoluteString];
////
////    [self max_sessionMemberId:navigationAction.request.URL];
////    //
////
//    if (![absStr hasPrefix:@"tel://"] && ![absStr containsString:@"wvjbscheme://"] &&  ![[absStr lowercaseString] containsString:@"register"]) {
////
//        if ([absStr rangeOfString:@"client_device=ios"].location == NSNotFound) {
//            if ([absStr rangeOfString:@"?"].location == NSNotFound && [absStr rangeOfString:@";"].location == NSNotFound) {
//                absStr = [NSString stringWithFormat:@"%@?client_device=ios",absStr];
//            }else{
//                    absStr = [NSString stringWithFormat:@"%@&client_device=ios",absStr];
////
//            }
//////
//            if ([absStr rangeOfString:@"userId"].location == NSNotFound && self.memberSessionSd != nil) {
//                if ([absStr rangeOfString:@"?"].location == NSNotFound && [absStr rangeOfString:@";"].location == NSNotFound) {
//                    absStr = [NSString stringWithFormat:@"%@?userId=%@",absStr,self.memberSessionSd];
//                }else{
//                    absStr = [NSString stringWithFormat:@"%@&userId=%@",absStr,self.memberSessionSd];
//                }
//
//            }
//            
//            if ([absStr rangeOfString:@"login"].location == NSNotFound && [absStr rangeOfString:@"toModifyBank"].location == NSNotFound) {
//                NSMutableURLRequest *asd = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[absStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
//////
//////
//                [webView loadRequest:asd];
////
//            }
//        }
//    }
////
////    [self alipaysClick:navigationAction.request.URL];
//    
//    if ([absStr hasPrefix:@"tel://"]) {
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:absStr]];
//        decisionHandler(WKNavigationActionPolicyCancel);
//        return;
//        
//    }
////    
////
////    //MARK: 调用退出
//    [self outLogin:navigationAction.request.URL.absoluteString];
////
////    if ([navigationAction.request.URL.absoluteString containsString:@"order/payment"]) {
////        
////        self.historyUrl = [self getOrderId:navigationAction.request.URL.absoluteString];
////        
////    }
////    
////    
////    //MARK: 退回
////    [self max_goBack:navigationAction.request.URL execution:^{decisionHandler(WKNavigationActionPolicyCancel);}];
//    
//
    
    decisionHandler(WKNavigationActionPolicyAllow);
}




#pragma mark - WKScriptMessageHandler

- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message
{
    if ([message.name isEqualToString:@"pay"]) {
        
        if (_showDelegate && [self.showDelegate respondsToSelector:@selector(getOrderIdAndPrice:)]) {
            [self.showDelegate getOrderIdAndPrice:message.body];
        }
        
    }

}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context{

    if ([keyPath isEqualToString:@"estimatedProgress"]) {
        CGFloat newprogress = [[change objectForKey:NSKeyValueChangeNewKey] doubleValue];
        
        if (self.showDelegate && [self.showDelegate respondsToSelector:@selector(currentProgressView:)]) {
            [self.showDelegate currentProgressView:newprogress];
        }
    }

//    [self evaluateJavaScript:@"loginMethod('13929115287','123456qqq')" completionHandler:^(id _Nullable obj, NSError * _Nullable error) {
    
        
//    }];
}



- (void)dealloc{

    [self removeObserver:self forKeyPath:@"estimatedProgress"];
}

-  (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{

    NSLog(@"asd");
}







@end



@implementation UIView(Cutter)

- (UIImage*)viewCutter
{
    UIGraphicsBeginImageContextWithOptions(self.bounds.size,NO,[[UIScreen mainScreen] scale]);
    
    // 方法一 有时导航条无法正常获取
    // [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    // 方法二 iOS7.0 后推荐使用
    [self drawViewHierarchyInRect:self.bounds afterScreenUpdates:YES];
    
    UIImage*img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return img;
}

@end



@implementation UIScrollView(Cutter)

- (UIImage *)scrollViewCutter
{
    //保存
    CGPoint savedContentOffset = self.contentOffset;
    CGRect savedFrame = self.frame;
    
    self.contentOffset = CGPointZero;
    self.frame = CGRectMake(0, 0, self.contentSize.width, self.contentSize.height);
    
    UIImage *image = [self viewCutter];
    
    //还原数据
    self.contentOffset = savedContentOffset;
    self.frame = savedFrame;
    
    return image;
    
    
}

@end


