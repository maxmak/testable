#import "NSObject+Extension.h"
#import "HttpRequest.h"
#import <objc/runtime.h>
#import "WXApi.h"
#import "ViewController.h"
@implementation WXInfo

+ (instancetype)initWithObj:(id)obj{
    
    WXInfo *model = [[WXInfo alloc] init];
    
    [model setValuesForKeysWithDictionary:obj];
    
    return model;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key{
    
}

- (id)valueForUndefinedKey:(NSString *)key{
    return nil;
}

@end


@implementation NSObject (Extension)

- (NSDictionary *)dictionaryRepresentation:(NSArray <NSString *>*)filter {
    
    NSMutableDictionary *props = [NSMutableDictionary dictionary];
    unsigned int outCount, i;
    objc_property_t *properties = class_copyPropertyList([self class], &outCount);
    for (i = 0; i<outCount; i++)
    {
        objc_property_t property = properties[i];
        const char* char_f =property_getName(property);
        NSString *propertyName = [NSString stringWithUTF8String:char_f];
        
        if ([filter containsObject:propertyName]) {
            continue;
        }
        
        id propertyValue = [self valueForKey:(NSString *)propertyName];
        
        if (propertyValue) [props setObject:(propertyValue ? propertyValue : @"") forKey:propertyName];
    }
    free(properties);
    return props;
}


+ (NSString *)getWXSchemes{
    
    
    NSArray * info = [[NSBundle mainBundle]infoDictionary][@"CFBundleURLTypes"];
    
    NSArray * wxSchemes = info.firstObject[@"CFBundleURLSchemes"];
    
    return wxSchemes[0];
}


- (NSString *)getWXUrlCode:(NSString *)code{

    NSString *secret = @"ab420bd568f7ac99d890e272641bbc1f";
    return [NSString stringWithFormat:@"https://api.weixin.qq.com/sns/oauth2/access_token?appid=wx8953829eeb1d980c&secret=%@&code=%@&grant_type=authorization_code",code,secret];
}

- (NSString *)getWXUrlToken:(NSString *)token toOpenid:(NSString *)openid{

    return [NSString stringWithFormat:@"https://api.weixin.qq.com/sns/userinfo?access_token=%@&openid=%@",token,openid];
}

#pragma mark - 微信登录

- (void)wechatLogin{

    SendAuthReq * req = [[SendAuthReq alloc] init];
    
    req.scope = @"snsapi_message,snsapi_userinfo";
    
    req.state = @"345";
    
    [WXApi sendAuthReq:req viewController:self delegate:[WXApiManager sharedManager]];

    [WXApiManager sharedManager].delegate = self;
    
}


#pragma mark - 微信分享

- (void)weixinShared:(BOOL)style dict:(NSDictionary *)dict {
    
    WXMediaMessage *message = [WXMediaMessage message];
    
    message.title = dict[@"title"] && [dict[@"title"] length] != 0 ?dict[@"title"]:@"朗姿青春日记";//
    message.description = dict[@"description"] && [dict[@"description"] length] != 0 ?dict[@"description"]:@"朗姿青春日记";//
    
    if (dict[@"icon"] && [dict[@"icon"] length] != 0)
        message.thumbData = UIImageJPEGRepresentation([UIImage imageWithData: [NSData dataWithContentsOfURL:[NSURL URLWithString:dict[@"icon"]]]], 0.1);
    else
        [message setThumbImage:[UIImage imageNamed:@"icon"]];
    
    
    WXWebpageObject *webpageObject = [WXWebpageObject object];
    
    webpageObject.webpageUrl = dict[@"url"] && [dict[@"url"] length] != 0?dict[@"url"]:[(ViewController *)self jieTu];//
    
    message.mediaObject = webpageObject;
    
    SendMessageToWXReq *req = [[SendMessageToWXReq alloc] init];
    
    req.bText = NO;

    req.message = message;
    
    req.scene = style ? WXSceneTimeline : WXSceneSession;
    
    [WXApi sendReq:req];
    
    [WXApiManager sharedManager].delegate = self;
}


- (void)managerDidRecvAuthResponse:(SendAuthResp *)response{
    
    if ([self respondsToSelector:@selector(WXLoginStart)]) {
        [self WXLoginStart];
    }
    
    NSError *error;
    switch (response.errCode) {
        case WXSuccess:
            [self requestWXAccessToken:response.code];
            break;
        default:
            //--
          error = [NSError errorWithDomain:@"" code:response.errCode userInfo:nil];
            if ([self respondsToSelector:@selector(WXLoginError:)]) {
                [self WXLoginError:error];
            }
            break;
    }
    

}



- (void)requestWXAccessToken:(NSString *)code{

    [HttpRequest GETwithURL:[self getWXUrlCode:code] params:nil headers:nil success:^(id responseObject) {
       
        NSString *token = responseObject[@"access_token"];
        NSString *openid = responseObject[@"openid"];
        [self requestWXInfoToken:token toOpenid:openid];
        
    } failure:^(NSError *error) {
        //--
        if ([self respondsToSelector:@selector(WXLoginError:)]) {
            [self WXLoginError:error];
        }
    }];
    
}


- (void)requestWXInfoToken:(NSString *)token toOpenid:(NSString *)openid{

    [HttpRequest GETwithURL:[self getWXUrlToken:token toOpenid:openid] params:nil headers:nil success:^(id responseObject) {
        WXInfo *model = [WXInfo initWithObj:responseObject];
        if ([self respondsToSelector:@selector(WXLoginSuccess:)]) {
            [self WXLoginSuccess:model];
        }
        
    } failure:^(NSError *error) {
        //--
        if ([self respondsToSelector:@selector(WXLoginError:)]) {
            [self WXLoginError:error];
        }
    }];
}

#pragma mark - 微信支付

- (void)wechatPay:(NSDictionary *)dict{
    
    PayReq *request = [[PayReq alloc] init];
    request.partnerId = dict[@"partnerId"];
    request.prepayId= dict[@"prepayId"];
    request.package = dict[@"package"];
    request.nonceStr= dict[@"nonceStr"];
    request.timeStamp= [dict[@"timeStamp"] intValue];
    request.sign= dict[@"sign"];
    [WXApi sendReq:request];
    
    [WXApiManager sharedManager].delegate = self;
}



@end




@implementation NSString (MD5)
// appid body
- (BOOL)stringDecideASCIIObject:(NSString *)object{
    
    NSUInteger frontLength = [self length];
    NSUInteger queenLength = [object length];
    
    NSUInteger currentLength = frontLength < queenLength ? frontLength : queenLength;
    
    for (int i = 0; i < currentLength; i++) {
        int frontIndex = [self characterAtIndex:i];
        int queenIndex = [object characterAtIndex:i];
        if (frontIndex != queenIndex) {
            return frontIndex > queenIndex;
        }
     
    }
    return false;
}


- (NSString *)MD5
{
         const char *cStr = [self UTF8String];
         unsigned char digest[16];
         unsigned int x=(int)strlen(cStr) ;
         CC_MD5( cStr, x, digest );
    
         NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
         for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
             [output appendFormat:@"%02x", digest[i]];
         return  output;
}

+(NSDictionary *)parseJSONStringToNSDictionary:(NSString *)JSONString {
    NSData *JSONData = [JSONString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *responseJSON = [NSJSONSerialization JSONObjectWithData:JSONData options:NSJSONReadingMutableLeaves error:nil];
    return responseJSON;
}
@end

@implementation NSDictionary (Extension)

- (NSString *)dictionaryToString{
    
    NSMutableArray<NSString *> *array = [NSMutableArray arrayWithCapacity:self.count];
    [self enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull key, NSString * _Nonnull obj, BOOL * _Nonnull stop) {
        [array addObject:[NSString stringWithFormat:@"%@=%@",key,obj]];
    }];
    
    for (int i = 0; i < array.count; i++) {
        for (int k = i + 1;k < array.count; k++) {
            
            NSString *frontKey = array[i];
            NSString *queenKey = array[k];
            
            if ([frontKey stringDecideASCIIObject:queenKey]) {
                [array exchangeObjectAtIndex:i withObjectAtIndex:k];
            }
        }
    }
    NSString *str = [array componentsJoinedByString:@"&"];
    
    return str;
}



@end

@implementation NSArray (Extension)

- (BOOL)contains:(NSString *)key{

    if (self == nil) {
        return false;
    }
    if (self.count == 0) {
        return false;
    }
    
    for (NSString *obj in self) {
        if ([obj isEqualToString:key]) return YES;
    }
    
    return false;
}

@end
