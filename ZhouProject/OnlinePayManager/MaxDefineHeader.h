//
//  MaxDefineHeader.h
//
//  Created by Max on 14/4/22.
//
 
#ifndef VIP_One_4_0_MaxDefineHeader_h
#define VIP_One_4_0_MaxDefineHeader_h
 
static NSString *kAuthScope = @"snsapi_message,snsapi_userinfo";
static NSString *kAuthState = @"345";


#pragma mark - NSNotification Center

#define kNotificationMessage @"MaxNotificationMessage"
#define kNotificationExitLogin @"MaxNotificationExitLogin"
#define kMeDataSexy @"MeDataSexy"
#define kResigerPushNotifications @"pushNotifications"
#define kMessageLocalNotifi @"MessageLocalNotifi"
#define kPushNotifictionToken @"pushNotificationToken"

#define kRegisiterSuccess @"kNotificationRegisiterSuccess"
#define kAliPaySuccess @"kNotificationAliPaySuccess"
#define kWechatPaySuccess @"kNotificationWechatPaySuccess"

#define kLoginSuccessNoti @"kNotificationLoginSuccess"
#define kExitSuccessNoti @"kNotificationExitSuccess"

#define kRechargeSuccessNoti @"kRechargeSuccessNoti"

#define kMessageBageHide @"kMessageBageHide"

#define kPasswordModifySuccess @"kNotificationPasswordModifySuccess"


#pragma mark - Tag
#define kMeCertionTopBtnTag (11700)
#define kTdTag (![self isKindOfClass:[MeCertificationGroupView class]]?20000:21000)
#define kCationSelectTag (![self isKindOfClass:[MeCertificationGroupView class]]?22000:23000)
#define kTabbarTag 10000
#define kcheckButtonTag 20010
#define kAddBankTextFeildTag 24000
#define kSharedButtonTag 25000
#define kSexButtonTag 26000

#pragma mark - 设备类型
#define kiPhone4     ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)

#define kiPhone5   ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)

#define kiPhone6     ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size) : NO)
#define kiPhone6Plus ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242, 2208), [[UIScreen mainScreen] currentMode].size) : NO)
 
 
#pragma mark - 设备信息

#define System_Version(version) ([[[UIDevice currentDevice] systemVersion] floatValue] >= version)//获取当前版本号，整数
#define kIOS_VERSION    [[[UIDevice currentDevice] systemVersion] floatValue]
#define kDEVICE_MODEL   [[UIDevice currentDevice] model]
#define kIS_IPAD        ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)//判断是否iPad
#define kIS_IPHONE        ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)//判断是否iPhone
#define kisRetina       ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)
#define kAPP_NAME            [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"]
#define kAPP_VERSION         [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]
#define kAPP_SUB_VERSION     [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]
#define kUDeviceIdentifier   [[UIDevice currentDevice] uniqueDeviceIdentifier]
 
 
#pragma mark - 常用宏定义
#define kWS(weakSelf)          __weak __typeof(&*self)weakSelf = self;//self 弱引用
#define KSS(var)               __strong typeof(var) strongSelf = var
#define STRONGSELF             typeof(weakSelf) __strong strongSelf = weakSelf;//强引用
#define kSCREEN_BOUNDS         ([UIScreen mainScreen].bounds)
#define kSCREEN_WIDTH          ([UIScreen mainScreen].bounds.size.width)
#define kSCREEN_HEIGHT         ([UIScreen mainScreen].bounds.size.height)
#define kUSER_DEFAULT          [NSUserDefaults standardUserDefaults]
#define kNOTIFICATION_DEFAULT  [NSNotificationCenter defaultCenter]
#define kIMAGENAMED(_pointer)  [UIImage imageNamed:[UIUtil imageName:_pointer]]//设置图片，带缓存
#define kLOADIMAGE(file,ext)   [UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:file ofType:ext]]//设置图片，不带缓存
 
#define kScreenWidthScaleSize           (MIN(SCREEN_WIDTH,SCREEN_HEIGHT)/320.0)
#define kScreenWidthScaleSizeByIphone6  (MIN(SCREEN_WIDTH,SCREEN_HEIGHT)/375.0)
 
#define kDegreesToRadian(x)         (M_PI * (x) / 180.0)
#define kRadianToDegrees(radian)    (radian*180.0)/(M_PI)


#define MaxFORMAT(fmt, ...) [NSString stringWithFormat:(fmt), ##__VA_ARGS__]//格式化字符串

 
 
#pragma mark - ios版本判断
 
#define kIOS5_OR_LATER   ( [[[UIDevice currentDevice] systemVersion] compare:@"5.0"] != NSOrderedAscending )
#define kIOS6_OR_LATER   ( [[[UIDevice currentDevice] systemVersion] compare:@"6.0"] != NSOrderedAscending )
#define kIOS7_OR_LATER   ( [[[UIDevice currentDevice] systemVersion] compare:@"7.0"] != NSOrderedAscending )
#define kIOS8_OR_LATER   ( [[[UIDevice currentDevice] systemVersion] compare:@"8.0"] != NSOrderedAscending )
#define kIOS9_OR_LATER   ( [[[UIDevice currentDevice] systemVersion] compare:@"9.0"] != NSOrderedAscending )
 
#pragma mark - 是否为空或是[NSNull null]
 
#define kNotNilAndNull(_ref)  (((_ref) != nil) && (![(_ref) isEqual:[NSNull null]]))
#define kIsNilOrNull(_ref)   (((_ref) == nil) || ([(_ref) isEqual:[NSNull null]]))


#pragma mark - 判断字符串是否为空
#define kStringNotNil(_ref) !([(_ref) isEqualToString:@""]) && (_ref) != nil
#define kStringNil(_ref) [(_ref) isEqualToString:@""] || ((_ref) == nil)
 
#pragma mark - 图片资源获取
#define kIMGFROMBUNDLE( X )     [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:X ofType:@""]]
#define kIMGNAMED( X )         [UIImage imageNamed:X]
 
#pragma mark - 颜色
#define kColorWhite [UIColor whiteColor]
#define CLEARCOLOR [UIColor clearColor]
#define kCOLOR_RGB(r,g,b)     [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:1.0]
#define kCOLOR_RGBA(r,g,b,a)  [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:(a)]
// rgb颜色转换（16进制->10进制）
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
#define kColorDef UIColorFromRGB(0xf5f5f5)

#define TEXTCOLOR [UIColor colorWithRed:49/255.f green:39/255.f blue:41/255.f alpha:1]
#define GOLDCOLOR [UIColor colorWithRed:218/255.f green:218/255.f blue:112/255.f alpha:1]
#define LINECOLOR [UIColor colorWithRed:230/255.f green:230/255.f blue:230/255.f alpha:1.0]
#define POINTCOLOR [UIColor colorWithRed:145/255.f green:144/255.f blue:145/255.f alpha:1.0]
#define NAVBARCOLOR [UIColor colorWithRed:27/255.f green:27/255.f blue:27/255.f alpha:1]

#define STOTAL     7
#define SPAY       8
#define SDELIVERY  9
#define SRECEIVE   10
#define SEVALUATE  11
#define SAFTERSALE 12

#define QQ_APIID @"101362740"
//#define kWechatKey (@"wx001536f563d404c5")
#define kWechatKey (@"wx8953829eeb1d980c")

#define kHeaderImagePath @"10187334567239"

#pragma mark - 状态栏，导航栏的高度
#define STATUS_Height [[UIApplication sharedApplication] statusBarFrame].size.height
#define NAVIGATION_Height 44
#define TOP_OFFSET 64

#define kNorBGColor kCOLOR_RGB(245, 245, 245)
 
#pragma mark - 定义字号
#define kFONT_TITLE(X)     [UIFont systemFontSize:X]
#define kFONT_CONTENT(X)   [UIFont systemFontSize:X]
 
 
#pragma mark - 其他
 
#define BINDVIEWWIDTH        (ScreenBounds.size.width * 290.0 / 320.0)

/**
 * 获取Library/Caches路径
 */
#define CACHEPATH ([NSString stringWithFormat:@"%@/Library/Caches", NSHomeDirectory()])

/**
 * 这是一个消除performSelector警告的宏
 */
#define clearWarning_preformSel(Stuff) \
do { \
_Pragma("clang diagnostic push") \
_Pragma("clang diagnostic ignored \"-Warc-performSelector-leaks\"") \
Stuff; \
_Pragma("clang diagnostic pop") \
} while (0)

/**
 * 这是一个强制消除警告的宏
 */
#define clearWarning_force(Stuff) \
do { \
_Pragma("clang diagnostic push") \
_Pragma("clang diagnostic ignored \"-Warc-bridge-casts-disallowed-in-nonarc\"") \
Stuff; \
_Pragma("clang diagnostic pop") \
} while (0)


/**
 * 回到主线程同步执行
 */
#define sync_main_safe(block) \
if ([NSThread isMainThread]) {\
block();\
} else {\
dispatch_sync(dispatch_get_main_queue(), block);\
}

/**
 * 回到主线程异步执行
 */
#define async_main_safe(block)\
if ([NSThread isMainThread]) {\
block();\
} else {\
dispatch_async(dispatch_get_main_queue(), block);\
}

#pragma mark - 重写NSLog
#ifdef DEBUG
#define NSLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#define DLog(...)
#endif

#define CustomNSLog(...) printf("%f %s\n",[[NSDate date]timeIntervalSince1970],[[NSString stringWithFormat:__VA_ARGS__]UTF8String]);

/**
 * 重写NSLog，Debug模式下打印日志和当前行数
 * 防止release版本中含有多余的打印信息
 */
//#if DEBUG
////#define NSLog(fmt, ...) fprintf(stderr,"\nfunc:%s line:%d content:%s\n", __FUNCTION__, __LINE__, [[NSString stringWithFormat:fmt, ##__VA_ARGS__] UTF8String]);
//#define NSLog(fmt, ...) fprintf(stderr,"[class:%s -- line:%04d]%s\n", [NSStringFromClass([self class]) UTF8String], __LINE__, [[NSString stringWithFormat:fmt, ##__VA_ARGS__] UTF8String]);
//#else
//#define NSLog(fmt, ...) nil
//#endif

#endif

