#import <WebKit/WebKit.h>
//static  NSString  *MAINURL = @"http://www.yibaobang.com/dsc/servlet//proxy/proxyMember/toMain";
static  NSString  *MAINURL = @"http://www.sqxtop.com";
@protocol ShowProgressViewDelegate <NSObject>

- (void)currentProgressView:(CGFloat)Progress;
- (void)webViewpayActionOrder:(NSString *)orderId price:(NSString *)price;

- (void)getOrderIdAndPrice:(id)msg;

- (void)sharedARCode:(BOOL)style dict:(NSDictionary *)dict;
- (void)getScanVC;
- (void)goError:(NSString *)url;
@end

@interface MainWebView : WKWebView

@property (weak,nonatomic) id<ShowProgressViewDelegate> showDelegate;
@property (assign,nonatomic,readonly) BOOL loadState;

+ (WKWebViewConfiguration *)getWKWebViewConfiguration;

+ (BOOL)isUrl:(NSString *)url;

- (BOOL)max_loadRequest:(NSString *)url;


- (void)alipayResults;
@end


@interface UIView (Cutter)
- (UIImage*)viewCutter;
@end

@interface UIScrollView (Cutter)
- (UIImage *)scrollViewCutter;
@end
