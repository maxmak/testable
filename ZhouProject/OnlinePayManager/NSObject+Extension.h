#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "WXApi.h"
#import "WXApiManager.h"
#import <CommonCrypto/CommonCrypto.h>

@interface WXInfo : NSObject
@property (nonatomic,strong) NSString *city;
@property (nonatomic,strong) NSString *country;
@property (nonatomic,strong) NSString *headimgurl;
@property (nonatomic,strong) NSString *language;
@property (nonatomic,strong) NSString *nickname;
@property (nonatomic,strong) NSString *openid;
//@property (nonatomic,strong) NSArray *privilege;
@property (nonatomic,strong) NSString *province;
@property (nonatomic,strong) NSNumber *sex;
@property (nonatomic,strong) NSString *unionid;

+ (instancetype)initWithObj:(id)obj;
@end


@interface NSObject (Extension)<WXApiDelegate,WXApiManagerDelegate>


/**
 遍历对象属性

 @param filter 过滤关键字
 @return 字典
 */
- (NSDictionary *)dictionaryRepresentation:(NSArray <NSString *>*)filter;

/**
 获取 微信Appid

 @return weixin appid
 */
+ (NSString *)getWXSchemes;


/**
 微信登录
 */
- (void)wechatLogin;

- (void)wechatPay:(NSDictionary *)dict;

- (void)weixinShared:(BOOL)style dict:(NSDictionary *)dict;

/**
 开始登陆微信
 */
- (void)WXLoginStart;

/**
 微信登录失败

 @param error 错误信息
 */
- (void)WXLoginError:(NSError *)error;

/**
 微信登录成功

 @param model 微信个人信息
 */
- (void)WXLoginSuccess:(WXInfo *)model;
@end

@interface NSString (MD5)
- (NSString *)MD5;
- (BOOL)stringDecideASCIIObject:(NSString *)object;

+(NSDictionary *)parseJSONStringToNSDictionary:(NSString *)JSONString;
@end

@interface NSDictionary (Extension)

/**
 字典转字符串

 @return 字符串
 */
- (NSString *)dictionaryToString;
@end

@interface NSArray (Extension)

@end

