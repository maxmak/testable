//
//  OnlinePayManager.h
//  ZhouProject
//
//  Created by MaxMak on 2017/8/24.
//  Copyright © 2017年 MaxMak. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TonglianPayModel;
typedef NS_ENUM(NSInteger, TongLianStyle) {
    
    pay = 0,
    cancel,
    refund,
    query,
    closes,
};


@interface OnlinePayManager : NSObject

+ (OnlinePayManager *)manager;
+ (NSString *)getTonglianUrlStyle:(TongLianStyle)style;

- (void)getTonglianPay:(TonglianPayModel *)model
               success:(void(^)(id responseObject))success
               failure:(void(^)(NSError *error))failure;


@end
