//
//  ViewController.h
//  ZhouProject
//
//  Created by MaxMak on 2017/8/24.
//  Copyright © 2017年 MaxMak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainWebView.h"
@interface ViewController : UIViewController

@property (readonly,nonatomic) MainWebView *webView;

- (NSString *)jieTu;
@end

