//
//  ViewController.m
//  ZhouProject
//
//  Created by MaxMak on 2017/8/24.
//  Copyright © 2017年 MaxMak. All rights reserved.
//

#import "ViewController.h"
#import "MainWebView.h"
#import "NSObject+Extension.h"
#import "HttpRequest.h"
#import "MBPHUBTool.h"
#import "MaxAlertViewManager.h"
#import "MBProgressHUD.h"
#import "OnlinePayManager.h"
#import "WXApi.h"

#import <CoreTelephony/CTCellularData.h>
#import <WebViewJavascriptBridge/WebViewJavascriptBridge.h>
#import "LunachScrollView.h"
#import <AlipaySDK/AlipaySDK.h>
#import "CustTools.h"
#import "MaxScanViewController.h"
#import <SGQRCode/SGQRCodeScanManager.h>
#import "HMScannerController.h"

#import "ErrorViewController.h"
//#import <SVProgressHUD/SVProgressHUD.h>
@interface ViewController ()<ShowProgressViewDelegate>

@property (strong,nonatomic) MainWebView *webView;
@property (strong,nonatomic) MBProgressHUD *hud;
@property (strong,nonatomic) UIProgressView *progressView;
@property (strong,nonatomic) CTCellularData *cellularData ;
//@property (strong,nonatomic) SVProgressHUD *svhud;
@property (nonatomic,strong) UIButton *errorBtn;

@end



@implementation ViewController


- (UIButton *)errorBtn{
    
    if (!_errorBtn) {
        _errorBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        
    }
    return _errorBtn;
}

- (MBProgressHUD *)hud{

    if (!_hud) {
       _hud = [[MBProgressHUD alloc] initWithView:self.view];
        _hud.mode = MBProgressHUDModeIndeterminate;
        [self.view addSubview:_hud];
        
//        _hud.contentColor = [UIColor colorWithRed:8/255.0 green:150/255.0 blue:247/255.0 alpha:1.0];
        _hud.contentColor = [UIColor redColor];
    }

    return _hud;
}

- (UIProgressView *)progressView
{
    if(!_progressView)
    {
        _progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(0, 20, self.view.frame.size.width, 5)];
//        self.progressView.tintColor = [UIColor colorWithRed:8/255.0 green:150/255.0 blue:247/255.0 alpha:1.0];
        _progressView.tintColor = [UIColor redColor];
        self.progressView.trackTintColor = [UIColor whiteColor];
        [self.view addSubview:self.progressView];
        
    }
    return _progressView;
}



- (MainWebView *)webView{

    if (!_webView) {
        _webView = [[MainWebView alloc] initWithFrame:CGRectMake(0, 20, self.view.frame.size.width, self.view.frame.size.height - 20) configuration:[MainWebView getWKWebViewConfiguration]];
        _webView.showDelegate = self;

    }
    return _webView;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
//    CEWebView *ce = [[CEWebView alloc] initWithFrame:CGRectMake(0, 20, self.view.frame.size.width, self.view.frame.size.height - 20)];
//    [self.view addSubview:ce];
    
    [self.view addSubview:self.webView];

    [self.webView max_loadRequest:MAINURL];

    [self.webView reload];

    self.automaticallyAdjustsScrollViewInsets = NO;
    
//    LunachScrollView *lunach = [[LunachScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
//    [self.view addSubview:lunach];
    
    //---
    
//    [self.view addSubview:self.lunach];
    
 
    
    
    
    [self firstStart];
    //----

}

#pragma mark - 首次启动 刷新 webview
- (void)firstStart{
    
    CTCellularData *cellularData = [[CTCellularData alloc]init];
    cellularData.cellularDataRestrictionDidUpdateNotifier =  ^(CTCellularDataRestrictedState state){
        //获取联网状态
        switch (state) {
            case kCTCellularDataRestricted:
                NSLog(@"Restricrted");
                break;
            case kCTCellularDataNotRestricted:
                NSLog(@"Not Restricted");
                
//                [self reload];
                break;
            case kCTCellularDataRestrictedStateUnknown:
                NSLog(@"Unknown");
                break;
            default:
                break;
        };
    };
    
}


- (void)reload{
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_global_queue(0, 0), ^{
        if (![[NSUserDefaults standardUserDefaults]boolForKey:@"start"]) {
           dispatch_async(dispatch_get_main_queue(), ^{
              
               [self.webView max_loadRequest:MAINURL];
               [self.webView reload];
               [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"start"];
               [[NSUserDefaults standardUserDefaults]synchronize];
               
           });
        }
    });
}


#pragma mark - 通联微信支付请求
- (void)pay:(TonglianPayModel * _Nonnull)model{
    [self.hud showAnimated:YES];
    
    [[OnlinePayManager manager]getTonglianPay:model success:^(id responseObject) {
//        //  {"appid":"00000006",
//             "cusid":"XXXXXXXX",
//             "weixinstr":"{\"partnerId \":\"10000100\",
//                           \"timeStamp\":\"1477356696\",
//                           \"prepayId\":\"1101000000140415649af9fc314aa427\",
//                           \"package\":\"Sign=WXPay\",
//                           \"nonceStr\":\"38642\",
//                           \"sign\":\"66FF000B739F459D093FE24AB3462170\"}",
//             "reqsn":"1610258923119024",
//             "retcode":"SUCCESS",
//             "sign":"9513D3ABF5983F3FACCD8161931DB1ED",
//             "trxid":"180681592",
//             "trxstatus":"0000"}
        if (![responseObject[@"retcode"] isEqualToString:@"SUCCESS"]) {
            [MaxAlertViewManager showalertViewMaessage:@"获取失败"];
            [self.hud hideAnimated:YES afterDelay:1.0];
            return ;
        }
        
        NSDictionary *weixinstr = [NSString parseJSONStringToNSDictionary:responseObject[@"weixinstr"]];
        //
        [self wechatPay:weixinstr];
        
    } failure:^(NSError *error) {
        [MaxAlertViewManager showalertViewMaessage:@"获取失败"];
        [self.hud hideAnimated:YES afterDelay:1.0];
    }];

}

- (void)goError:(NSString *)url{
    
    if (self.webView.loadState) {
        return;
    }
    
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    ErrorViewController *vc = [story instantiateViewControllerWithIdentifier:@"errorStyID"];
    
    [self presentViewController:vc animated:NO completion:nil];
    
    [[NSNotificationCenter defaultCenter]addObserverForName:@"errorKey" object:nil queue:NSOperationQueue.mainQueue usingBlock:^(NSNotification * _Nonnull note) {
       
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.webView max_loadRequest:url];
            
            [self.webView reload];
            
        });
    }];
    
}

- (void)managerDidRecvPayResponse:(PayResp *)response{
 
    [self.hud hideAnimated:YES afterDelay:1.0];
    //MARK: TEST
    
    
}

#pragma mark - WKWebView 加载进度
- (void)currentProgressView:(CGFloat)Progress{
    
    [self.progressView setHidden:(Progress == 1 ? YES : NO)];
    [self.progressView setProgress:(Progress == 1? 0 : Progress) animated:YES];
    if (Progress != 1) {
        [self.hud showAnimated:YES];
    }else{
        [self.hud hideAnimated:YES];
    }
    
    
}

#pragma mark -  支付按钮回调
- (void)webViewpayActionOrder:(NSString *)orderId price:(NSString *)price{
    
//    TonglianPayModel *model = [TonglianPayModel initWithTrxamt:@"1" toReqsn:@"1450432107647" toRandomstr:@"1450432107647" toBody:@"ceshiBody"];
    
//    [self pay:model];
}

#pragma mark - 获取单号和金额// window.webkit.messageHandlers.pay.postMessage({orderId:'',price:''});
- (void)getOrderIdAndPrice:(id)msg{

    
}


//#pragma mark - 开始 微信登录
//- (void)WXLoginStart{
//    
//    [self.hud showAnimated:YES];
//    
//
//}
//
//#pragma mark - 微信登录成功
//- (void)WXLoginSuccess:(WXInfo *)model{
//    
//    [self.hud hideAnimated:YES afterDelay:1.0];
//
//}
//
//#pragma mark - 微信登录失败
//- (void)WXLoginError:(NSError *)error{
//    
//    [self.hud hideAnimated:YES afterDelay:1.0];
//    [MaxAlertViewManager showalertViewMaessage:@"获取失败"];
//}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
//    [self weixinShared:NO dict:nil];
    

    
    
    
//    [self getScanVC];

    

}


- (void)getScanVC{
//
    
    __weak typeof(self)weakSelf = self;
    
    HMScannerController *scanner = [HMScannerController scannerWithCardName:@"" avatar:nil completion:^(NSString *stringValue) {
        
        NSLog(@"%@",stringValue);
        [weakSelf.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:stringValue]]];
        [weakSelf.webView reload];
        
    }];
    
    [scanner setTitleColor:[UIColor whiteColor] tintColor:[UIColor greenColor]];
    
    [self showDetailViewController:scanner sender:nil];
    
    
}


- (void)sharedARCode{

    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"分享" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction * cannel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    UIAlertAction * sharedFriend = [UIAlertAction actionWithTitle:@"分享好友" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
         [self weixinShared:NO dict:@{@"url":[self jieTu]}];
    }];
    
    UIAlertAction * sharedQuan = [UIAlertAction actionWithTitle:@"分享朋友圈" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//        [self jieTu];
        [self weixinShared:YES dict:@{@"url":[self jieTu]}];
    }];
    
    [alert addAction:cannel];
    [alert addAction:sharedQuan];
    [alert addAction:sharedFriend];
    [self presentViewController:alert animated:YES completion:nil];

}





- (NSString *)jieTu{

    UIImage *image = [self.webView.scrollView scrollViewCutter];

    NSString *code = [CustTools scanQRCode:image];
    
    return code;
    
}



- (void)sharedARCode:(BOOL)style dict:(NSDictionary *)dict {

    [self weixinShared:style dict:dict];

}


- (void)managerDidRecvMessageResponse:(SendMessageToWXResp *)response{

    if (response.errCode == 0) {
        [MBPHUBTool HudShowWithText:@"分享成功" toView:self.view];
    }else if (response.errCode == -2){
        
        [MBPHUBTool HudShowWithText:@"取消分享" toView:self.view];
    }else{
        [MBPHUBTool HudShowWithText:@"分享失败" toView:self.view];
    }

}

- (void)dealloc{

    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

@end
