//
//  CustTools.h
//  ZhouProject
//
//  Created by MaxMak on 2017/9/7.
//  Copyright © 2017年 MaxMak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface CustTools : NSObject
+ (UIImage *) captureScreen;
+ (NSString *)scanQRCode:(UIImage *)image;
@end
