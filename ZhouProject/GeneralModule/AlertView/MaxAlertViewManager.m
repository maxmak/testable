//
//  MaxAlertViewManager.m
//  WatchBank
//
//  Created by Maxs on 16/10/9.
//  Copyright © 2016年 Max Mak. All rights reserved.
//

#import "MaxAlertViewManager.h"

@implementation MaxAlertViewManager

/**
 *  创建一个Aler
 *
 *  @param title
 *  @param message
 *
 *  @return
 */
+ (UIAlertController *)alerViewTitle:(NSString *)title message:(NSString *)message
{
    UIAlertController *aler = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    return aler;
}

+ (UIAlertController *)alerSheetTitle:(NSString *)title message:(NSString *)message
{
    UIAlertController *aler = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleActionSheet];
    return aler;
}

/**
 *  返回一个取消按钮
 *
 *  @param handle 取消回调
 *
 *  @return
 */
+ (UIAlertAction *)canCelActionHandle:(void(^)(UIAlertAction *))handle
{
    if (handle) {
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:handle];
        return cancel;
    }
    return nil;
}

+ (void)showAlerCancelNormalWithTitle:(NSString *)title message:(NSString *)message employController:(UIViewController *)VC
{
    UIAlertController *aler = [[self class] alerViewTitle:title message:message];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [aler addAction:cancelAction];
    [VC presentViewController:aler animated:YES completion:nil];
}



+ (void)showAlerWithTitle:(NSString *)title message:(NSString *)message employController:(UIViewController *)VC handler:(void (^)(UIAlertAction *))handle
{
    UIAlertController *aler = [[self class] alerViewTitle:title message:message];
    
    UIAlertAction *cancelAction = [[self class] canCelActionHandle:^(UIAlertAction *action) {
        
    }];
    
    [aler addAction:cancelAction];
    
    UIAlertAction *sureAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (handle) {
            handle(action);
        }
    }];
    
    [aler addAction:sureAction];
    
    [VC presentViewController:aler animated:YES completion:nil];
}

+ (void)showAlerWithTextFileTitle:(NSString *)title message:(NSString *)message employController:(UIViewController *)VC handler:(void (^)(UIAlertAction *, UITextField *))handle
{
    UIAlertController *aler = [[self class] alerViewTitle:title message:message];
    
    UIAlertAction *cancelAction = [[self class] canCelActionHandle:^(UIAlertAction *action) {
        
    }];
    [aler addAction:cancelAction];
    
    UIAlertAction *sureAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        UITextField *texfield = [aler.textFields firstObject];
        if (handle) {
            handle(action,texfield);
        }
    }];
    [aler addAction:sureAction];
    
    [aler addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        
    }];
    
    [VC presentViewController:aler animated:YES completion:nil];
}

+(void)showActionWithMessage:(NSString *)message employController:(UIViewController *)VC handler:(void (^)(UIAlertAction *))handle
{
//    UIAlertController *alertController = [[self class] alerViewTitle:title message:message];
//    UIAlertAction *destroyAction = [UIAlertAction actionWithTitle:message
//                                                            style:UIAlertActionStyleDestructive
//                                                          handler:^(UIAlertAction *action) {
//                                                              // do destructive stuff here
//                                                          }];
    UIAlertAction *cancelAction = [[self class] canCelActionHandle:^(UIAlertAction *action) {
        
    }];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:  UIAlertControllerStyleActionSheet];
   
    [alert addAction:[UIAlertAction actionWithTitle:message style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (handle) {
            handle(action);
        }
    }]];
    
    [alert addAction:cancelAction];
    
    [VC presentViewController:alert animated:YES completion:nil];
  
}

+ (void)showAlertControllerStyleActionSheetWithTitle:(NSString *)title message:(NSString *)message employController:(UIViewController *)VC handler:(void (^)(UIAlertAction *))handle
{
    UIAlertController *aler = [[self class] alerSheetTitle:title message:message];
    
    UIAlertAction *cancelAction = [[self class] canCelActionHandle:^(UIAlertAction *action) {
        
    }];
    //修改字体颜色
    [cancelAction setValue:[UIColor redColor] forKey:@"_titleTextColor"];
    [aler addAction:cancelAction];
    
    UIAlertAction *sureAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (handle) {
            handle(action);
        }
    }];
    
    [aler addAction:sureAction];
    
    [VC presentViewController:aler animated:YES completion:nil];
}

+ (void)showArrayTitleSheetWithTitle:(NSString *)title message:(NSString *)message employController:(UIViewController *)VC WithTitleArray:(NSArray *)titles handler:(void (^)(UIAlertAction *, NSInteger))handle
{
    UIAlertController *aler = [[self class] alerSheetTitle:title message:message];
    UIAlertAction *cancelAction = [[self class] canCelActionHandle:^(UIAlertAction *action) {
        
    }];
    
    //修改字体颜色
    [cancelAction setValue:[UIColor redColor] forKey:@"_titleTextColor"];
    [aler addAction:cancelAction];
    
    for (int i = 0; i < titles.count; i++) {
        UIAlertAction *sureAction = [UIAlertAction actionWithTitle:titles[i] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            if (handle) {
                handle(action,i);
            }
        }];
        [aler addAction:sureAction];
    }
    
    [VC presentViewController:aler animated:YES completion:nil];
    
}


+ (void)showalertViewMaessage:(NSString *)message
{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    [alert show];
    
    
    
    
    
}

+ (void)showViewController:(UIViewController *)viewController title:(NSArray *)titleArr ComeraBlock:(void(^)())comera PhotoLibrary:(void(^)())library{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"选择" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *cannel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
        [alert.view removeFromSuperview];
        
    }];
    
    UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:[titleArr firstObject] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        comera();
        
    }];
    UIAlertAction *libraryAction = [UIAlertAction actionWithTitle:[titleArr lastObject] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        library();
    }];
    [alert addAction:cannel];
    [alert addAction:cameraAction];
    [alert addAction:libraryAction];
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
       
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            
        }else{
        
            [viewController presentViewController:alert animated:YES completion:^{
                
            }];

        }
        
        
    });

}

+ (void)showComeraViewController:(UIViewController * __nonnull __weak)viewController completion:(void(^)(UIImagePickerControllerSourceType type))completion{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"选择" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *cannel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
        [alert.view removeFromSuperview];
        
    }];
    
    UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
       
        if (completion) {
            completion(UIImagePickerControllerSourceTypePhotoLibrary);
        }
    }];
    UIAlertAction *libraryAction = [UIAlertAction actionWithTitle:@"相机" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (completion) {
            completion(UIImagePickerControllerSourceTypeCamera);
        }
    }];
    [alert addAction:cannel];
    [alert addAction:cameraAction];
    [alert addAction:libraryAction];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [viewController presentViewController:alert animated:YES completion:^{
            
        }];
    });

}

@end
