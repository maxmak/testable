//
//  HttpRequest.m
//  WatchBank
//
//  Created by Maxs on 16/10/10.
//  Copyright © 2016年 Max Mak. All rights reserved.
//

#import "HttpRequest.h"
#import "AFNetworking.h"
@implementation HttpRequest


+ (void)GETwithURL:(NSString *)url params:(NSDictionary *)params headers:(NSArray<NSDictionary *> *)headers success:(void (^)(id))success failure:(void (^)(NSError *))failure{

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    /**
     *有时候做Server那邊的人沒把head內的 meta的content格式指定好，需要加上这个
     *
     **/
    manager.requestSerializer.timeoutInterval = 10.0;
    if (headers.count>0) {
        for (NSDictionary *dic in headers) {
            [manager.requestSerializer setValue:[[dic allValues] lastObject] forHTTPHeaderField:[[dic allKeys] lastObject]];
        }
    }
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/html",@"text/plain",@"text/json",@"text/javascript",nil];
    
    
    [manager GET:url parameters:params progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
        }
    }];

    
    
    
}

+ (void)POSTJsonWithURL:(NSString *)url params:(NSDictionary *)params success:(void (^)(id))success failure:(void (^)(NSError *))failure{


    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    // 设置请求格式
    manager.requestSerializer.timeoutInterval = 10.0;
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    // 设置返回格式
    //    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    /**
     *有时候做Server那邊的人沒把head內的 meta的content格式指定好，需要加上这个
     *
     **/
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/html",@"text/plain",@"text/json",@"text/javascript",@"text/html",nil];
    [manager POST:url parameters:params progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
        }
    }];

    
}

+ (void)POSTwithURL:(NSString *)url params:(NSDictionary *)params success:(void (^)(id))success failure:(void (^)(NSError *))failure{

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    /**
     *有时候做Server那邊的人沒把head內的 meta的content格式指定好，需要加上这个
     *
     **/
    manager.requestSerializer.timeoutInterval = 10.0;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/html",@"text/plain",@"text/json",@"text/javascript",nil];
//    manager.requestSerializer = [AFJSONRequestSerializer serializer];
//    [manager.securityPolicy setAllowInvalidCertificates:YES];
    
    
    [manager POST:url parameters:params progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (success) {
            success(responseObject);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
        }
    }];
    



}

#pragma mark - 网络监听
+ (void)checkReachabilityStatusChange{

    AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
    [manager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        BOOL sta = YES;
        switch (status) {
            case AFNetworkReachabilityStatusUnknown:
                break;
            case AFNetworkReachabilityStatusNotReachable:
                sta = NO;

                break;
            case AFNetworkReachabilityStatusReachableViaWWAN:
                sta = YES;
                break;
            case AFNetworkReachabilityStatusReachableViaWiFi:
                sta = YES;
                break;
            default:
                break;
        }
        
        [[NSNotificationCenter defaultCenter]postNotificationName:kNetWorkingStatus object:@(sta)];
        
    }];

    [manager startMonitoring];
}

+(void)stopReachabilityStatusChange
{
    AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
    [manager stopMonitoring];
}


+(void)reachabilityNetworking{
 
    AFNetworkReachabilityManager *manger = [AFNetworkReachabilityManager sharedManager];
    
    [manger startMonitoring];
    //2.监听改变
    [manger setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {

        switch (status) {
        case          AFNetworkReachabilityStatusUnknown:
        case     AFNetworkReachabilityStatusNotReachable:
        [[NSNotificationCenter defaultCenter]postNotificationName:kReachability object:@(NO)];
                break;
        case AFNetworkReachabilityStatusReachableViaWWAN:
        case AFNetworkReachabilityStatusReachableViaWiFi:
        [[NSNotificationCenter defaultCenter]postNotificationName:kReachability object:@(YES)];
                break;
            default:
                break;
        }
        
        
        
    }];
}

@end
