//
//  HttpRequest.h
//  WatchBank
//
//  Created by Maxs on 16/10/10.
//  Copyright © 2016年 Max Mak. All rights reserved.
//

#import <Foundation/Foundation.h>


#define  kGetWXCode(code) ([NSString stringWithFormat:@"https://api.weixin.qq.com/sns/oauth2/access_token?appid=wx8953829eeb1d980c&secret=ab420bd568f7ac99d890e272641bbc1f&code=%@&grant_type=authorization_code",code])


#define kGetUesrInfo(token,openid) ([NSString stringWithFormat:@"https://api.weixin.qq.com/sns/userinfo?access_token=%@&openid=%@",token,openid])

#define kNetWorkingStatus @"NetWorkingStatusNotifi"

#define kReachability  @"kreachabilityNetworking"
@interface HttpRequest : NSObject

/**
 *  普通Get请求
 *
 *  @param url     地址
 *  @param params  参数
 *  @param success 请求成功回调
 *  @param failure 失败回调
 */
+ (void)GETwithURL:(NSString *)url params:(NSDictionary *)params headers:(NSArray <NSDictionary*>*)headers success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;
/**
 *  普通POST请求
 *
 *  @param url     地址
 *  @param params  参数
 *  @param success 成功回调
 *  @param failure 失败回调
 */
+ (void)POSTwithURL:(NSString *)url params:(NSDictionary *)params success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;
/**
 *  提交Jason数据的POST请求
 *
 *  @param url     地址
 *  @param params  Jason数据
 *  @param success 成功回调
 *  @param failure 失败回调
 */
+ (void)POSTJsonWithURL:(NSString *)url params:(NSDictionary *)params success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;


+ (void)checkReachabilityStatusChange;
+(void)stopReachabilityStatusChange;

+(void)reachabilityNetworking;



@end
