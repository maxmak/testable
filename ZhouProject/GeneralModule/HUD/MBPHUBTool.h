//
//  MBPHUBTool.h
//  WatchBank
//
//  Created by Maxs on 16/10/11.
//  Copyright © 2016年 Max Mak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
@interface MBPHUBTool : NSObject
+ (void)HudShowWithText:(NSString *_Nullable)text toView:(UIView *_Nullable)view;

+ (MBProgressHUD *_Nonnull)max_HUDShowtoView:(UIView * __weak _Nonnull)view;
+ (void)max_HUDShowjuhuatoView:(UIView *)view  showHUD:(void(^)(MBProgressHUD *hud))hud;
@end
