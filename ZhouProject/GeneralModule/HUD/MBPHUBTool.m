//
//  MBPHUBTool.m
//  WatchBank
//
//  Created by Maxs on 16/10/11.
//  Copyright © 2016年 Max Mak. All rights reserved.
//

#import "MBPHUBTool.h"

@implementation MBPHUBTool
+ (void)HudShowWithText:(NSString *_Nullable)text toView:(UIView *_Nullable)view
{
    if (!view) {
        return;
    }

    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.label.text = text;
        hud.removeFromSuperViewOnHide = YES;
    [hud hideAnimated:YES afterDelay:1.5f];
}


+ (MBProgressHUD *_Nonnull)max_HUDShowtoView:(UIView * __weak _Nonnull)view{

    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    
    return hud;
}

+ (void)max_HUDShowjuhuatoView:(UIView *)view  showHUD:(void(^)(MBProgressHUD *hud))hud{


    MBProgressHUD *hudMBP = [[MBProgressHUD alloc] initWithView:view];
    hudMBP.mode = MBProgressHUDModeIndeterminate;
    [view addSubview:hudMBP];
    [hudMBP showAnimated:YES];
    hud(hudMBP);

}

@end
