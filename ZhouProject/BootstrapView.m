//
//  BootstrapView.m
//  WatchBank
//
//  Created by Maxs on 12/6/16.
//  Copyright © 2016 Max Mak. All rights reserved.
//

#import "BootstrapView.h"

static  NSString *kLaunchImageKey = @"kLaunchImageKey";

@interface BootstrapView ()<UIScrollViewDelegate>
@property (nonatomic,strong)NSMutableArray <UIImage *> *imageDataList;
@property (nonatomic,strong)UIButton *emtryButton;
@property (nonatomic,strong)UIPageControl *pageControl;
@property (nonatomic,strong)UIScrollView *scrollView;


@end


@implementation BootstrapView



- (UIPageControl *)pageControl{

    if (!_pageControl) {
        _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, self.frame.size.height-60, self.frame.size.width, 30)];
        _pageControl.currentPage = 0;
        _pageControl.numberOfPages = self.imageDataList.count;
    }
    return _pageControl;
}

- (UIScrollView *)scrollView{

    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.showsHorizontalScrollIndicator = NO;
        
        _scrollView.delegate = self;
        
        _scrollView.bounces = NO;
        _scrollView.pagingEnabled = YES;
        _scrollView.contentSize = CGSizeMake(self.frame.size.width * self.imageDataList.count, 0);
        
        for (int i = 0; i < self.imageDataList.count; i++) {
            
            UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(0, i * self.frame.size.width, self.frame.size.width, self.frame.size.height)];
            [_scrollView addSubview:image];
        }
    }

    return _scrollView;
}

- (NSMutableArray <UIImage *>*)imageDataList{

    if (!_imageDataList) {
        _imageDataList = [NSMutableArray arrayWithCapacity:3];
        
        for (NSInteger i = 1; i <= 3; i++) {
            
            NSString * str = [NSString stringWithFormat:@"image_%ld",i];
            
            UIImage *image = [UIImage imageNamed:str];
            [_imageDataList addObject:image];
            
        }
    }
    return _imageDataList;
}

//- (UIButton *)emtryButton{
//
//    if (!_emtryButton) {
//        _emtryButton = [UIButton buttonWithType:UIButtonTypeCustom];
//        _emtryButton.frame =  [BootstrapView buttonFrame];
//        [_emtryButton addTarget:self action:@selector(emtryAction) forControlEvents:UIControlEventTouchUpInside];
//    }
//    
//    return _emtryButton;
//}




- (instancetype)initWithFrame:(CGRect)frame{

    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.scrollView];
        [self addSubview: self.pageControl];
        }
    return self;

}



//




- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{

//    NSInteger index = scrollView.contentOffset.x/kSCREEN_WIDTH;
//    self.pageControl.currentPage = index;
}

@end
