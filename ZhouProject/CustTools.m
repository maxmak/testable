//
//  CustTools.m
//  ZhouProject
//
//  Created by MaxMak on 2017/9/7.
//  Copyright © 2017年 MaxMak. All rights reserved.
//

#import "CustTools.h"
#import <QuartzCore/QuartzCore.h>
@implementation CustTools

+ (UIImage *) captureScreen {
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    CGRect rect = [keyWindow bounds];
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [keyWindow.layer renderInContext:context];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIImageWriteToSavedPhotosAlbum(img,nil,nil,nil);
    return img;
}

+ (NSString *)scanQRCode:(UIImage *)image{
    
    CIDetector*detector = [CIDetector detectorOfType:CIDetectorTypeQRCode context:nil options:@{ CIDetectorAccuracy : CIDetectorAccuracyHigh }];
    //2. 扫描获取的特征组
    NSArray *features = [detector featuresInImage:[CIImage imageWithCGImage:image.CGImage]];
    //3. 获取扫描结果
    CIQRCodeFeature *feature = [features objectAtIndex:0];
    NSString *scannedResult = feature.messageString;
    
    return scannedResult;
    
}

@end
