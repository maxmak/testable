//
//  BootstrapView.h
//  WatchBank
//
//  Created by Maxs on 12/6/16.
//  Copyright © 2016 Max Mak. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^finishBlock)();

@interface BootstrapView : UIView

+ (void)firstStartView;


@end
