//
//  LunachScrollView.m
//  ZhouProject
//
//  Created by MaxMak on 2017/9/5.
//  Copyright © 2017年 MaxMak. All rights reserved.
//

#import "LunachScrollView.h"


#define kiPhone4     ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)

#define kiPhone5   ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)

#define kiPhone6     ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size) : NO)
#define kiPhone6Plus ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242, 2208), [[UIScreen mainScreen] currentMode].size) : NO)


@interface LunachScrollView ()<UIScrollViewDelegate>
@property (strong,nonatomic) NSMutableArray <UIImage *>*imageArray;
@property (strong,nonatomic) UIPageControl *pageControl;
@property (strong,nonatomic) UIButton *sender;
@property (strong,nonatomic) UIScrollView *scrollView;
@property (strong,nonatomic) void(^block)();
@end

@implementation LunachScrollView


- (UIScrollView *)scrollView{

    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 20, self.frame.size.width, self.frame.size.height - 20)];
        
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.showsHorizontalScrollIndicator = NO;
        
        _scrollView.delegate = self;
        
        _scrollView.bounces = NO;
        _scrollView.pagingEnabled = YES;
        _scrollView.contentSize = CGSizeMake(self.frame.size.width * self.imageArray.count, self.frame.size.height - 20);
        
        for (int i = 0; i < self.imageArray.count; i++) {
            
            UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width * i, 0, self.frame.size.width, self.frame.size.height -20)];
            image.image = self.imageArray[i];
            
            [_scrollView addSubview:image];
        }

    }

    return _scrollView;
}


- (UIButton *)sender{

    if (!_sender) {
        _sender = [UIButton buttonWithType:UIButtonTypeCustom];
        [_sender addTarget:self action:@selector(actionClick) forControlEvents:UIControlEventTouchUpInside];
//        _sender.backgroundColor = [UIColor redColor];
        if (kiPhone6) {
            _sender.frame = CGRectMake(self.frame.size.width * 2 + 100 , 550, 160, 40);
        }else if (kiPhone5){
            _sender.frame = CGRectMake(self.frame.size.width * 2 +100 , 470, 120, 40);
            
        }
        else if (kiPhone6Plus){
            
            _sender.frame = CGRectMake(self.frame.size.width * 2 + 120 , 620, 160, 40);
            
        }else if (kiPhone4){
            _sender.frame = CGRectMake( self.frame.size.width * 2 +100 , 400, 120, 40);
        }else{
            _sender.frame =CGRectMake( self.frame.size.width * 2  , 0, self.frame.size.width, self.frame.size.height);
        }
        
    }

    return _sender;
}

- (NSMutableArray <UIImage *>*)imageArray{


    if (!_imageArray) {
        _imageArray = [NSMutableArray array];
        
        [_imageArray addObject:[UIImage imageNamed:@"image_1"]];
        [_imageArray addObject:[UIImage imageNamed:@"image_2"]];
        [_imageArray addObject:[UIImage imageNamed:@"image_3"]];
    }
    return _imageArray;
}


- (UIPageControl *)pageControl{

    if (!_pageControl) {
        _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, self.frame.size.height - 40, self.frame.size.width, 30)];
        _pageControl.currentPage = 0;
        _pageControl.numberOfPages = self.imageArray.count;
        _pageControl.pageIndicatorTintColor = [UIColor blackColor];
        _pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];
    }

    return _pageControl;
}


- (instancetype)initWithFrame:(CGRect)frame actionBlock:(void(^)())block{

    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.scrollView];
        
        [self addSubview:self.pageControl];
        self.block = block;
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame{

    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.scrollView];

        [self addSubview:self.pageControl];
        
        [self.scrollView addSubview:self.sender];
    }
    return self;
}


+ (void)startLook{
    
    LunachScrollView *lunach = [[LunachScrollView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    lunach.backgroundColor = [UIColor redColor];
    [[UIApplication sharedApplication].keyWindow addSubview:lunach];
    

}

- (void)actionClick{

    if (self.block) {
        self.block();
    }
    
    [UIView animateWithDuration:1.0 animations:^{
        self.transform=CGAffineTransformMakeScale(1.3f, 1.3f);
        self.alpha = 0.f;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
        NSInteger index = scrollView.contentOffset.x/self.frame.size.width;
        self.pageControl.currentPage = index;
}


@end
