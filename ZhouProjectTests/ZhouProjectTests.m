//
//  ZhouProjectTests.m
//  ZhouProjectTests
//
//  Created by MaxMak on 2017/8/24.
//  Copyright © 2017年 MaxMak. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MainWebView.h"
#import "TonglianPayModel.h"
#import "NSObject+Extension.h"
#import "OnlinePayManager.h"
#import "CustTools.h"
@interface ZhouProjectTests : XCTestCase

@end

@implementation ZhouProjectTests


- (void)testDealDict{

    TonglianPayModel *model = [TonglianPayModel initWithTrxamt:@"" toReqsn:@"" toRandomstr:@"" toBody:@""];
    
    NSDictionary *dict = [model dictionaryRepresentation:@[@"sign"]];
    NSLog(@"%@",dict);
    XCTAssertEqual(model.remark, dict[@"remark"]);
    

}

- (void)testDictionaryToString{

    NSString *str = [@{@"appid":@"111111",@"kasd":@"111111",@"bbb":@"111111"} dictionaryToString];
    
    XCTAssert([@"appid=111111&bbb=111111&kasd=111111" isEqualToString:str]);
    
}

- (void)testMd5{

    NSString *str = @"appid=00000000&body=商品名称&cusid=990440153996000&key=43df939f1e7f5c6909b3f4b63f893994&paytype=0&randomstr=1450432107647&remark=备注信息&reqsn=1450432107647&trxamt=1";
    
    NSString *upStr = [[str MD5]uppercaseString];
    
    NSLog(@"%@",upStr);
    
    XCTAssert([@"1918CC7DBBD120B1BB130C9400186F79" isEqualToString:upStr]);
}

- (void)testTonglianModel{
    
    TonglianPayModel *model = [TonglianPayModel initWithTrxamt:@"1" toReqsn:@"1450432107647" toRandomstr:@"1450432107647" toBody:@"ceshiBody"];
    NSLog(@"%@",model.sign);
    
    NSDictionary *dict = [model dictionaryRepresentation:nil];

    XCTAssert([model.sign isEqualToString:dict[@"sign"]]);

}



-(void)testStringToDictionary{

    NSDictionary *dict = [NSString parseJSONStringToNSDictionary:@"{\"partnerId \":\"10000100\",\"timeStamp\":\"1477356696\",\"prepayId\":\"1101000000140415649af9fc314aa427\",\"package\":\"Sign=WXPay\",\"nonceStr\":\"38642\",\"sign\":\"66FF000B739F459D093FE24AB3462170\"}"];
    NSLog(@"%@",dict);
    XCTAssert([@"66FF000B739F459D093FE24AB3462170" isEqualToString:dict[@"sign"]]);
    
}


-(void)testStringBi{

    
    NSString *front = @"appid";
    NSString *queen = @"body";
    
    XCTAssertFalse([front stringDecideASCIIObject:queen]);
    
    NSString *front1 = @"reqsn";
    NSString *queen1 = @"randomstr";

    XCTAssert([front1 stringDecideASCIIObject:queen1]);
}

- (void)testTonglian{

//    [TonglianPayModel getTongLianConfig];


}


- (void)testScanQRCode{


    UIImage *image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"IMG_0099.PNG" ofType:nil]];
    
    NSString * str = [CustTools scanQRCode:image];
    NSLog(@"%@",str);


}

@end
